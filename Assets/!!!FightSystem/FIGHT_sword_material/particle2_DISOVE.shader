// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.4901961,fgcg:0.4627451,fgcb:0.4117647,fgca:1,fgde:0.002,fgrn:-3.9,fgrf:487.62,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:True,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4795,x:33013,y:32707,varname:node_4795,prsc:2|emission-2393-OUT,clip-6328-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:32134,y:32621,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:214f7e62633ee4585a55b459cc6d0f53,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2393,x:32470,y:32570,varname:node_2393,prsc:2|A-6074-RGB,B-797-RGB,C-2886-RGB;n:type:ShaderForge.SFN_Color,id:797,x:32134,y:32431,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8113208,c2:0.3865255,c3:0.3865255,c4:1;n:type:ShaderForge.SFN_Multiply,id:798,x:32598,y:32800,varname:node_798,prsc:2|A-6074-A,B-2886-A;n:type:ShaderForge.SFN_Time,id:3976,x:31356,y:33288,varname:node_3976,prsc:2;n:type:ShaderForge.SFN_Sin,id:2952,x:31814,y:33348,varname:node_2952,prsc:2|IN-6995-OUT;n:type:ShaderForge.SFN_Abs,id:865,x:31996,y:33377,varname:node_865,prsc:2|IN-2952-OUT;n:type:ShaderForge.SFN_Slider,id:1493,x:31235,y:33452,ptovrint:False,ptlb:time_frequency,ptin:_time_frequency,varname:node_1493,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3756581,max:1;n:type:ShaderForge.SFN_Multiply,id:6995,x:31643,y:33376,varname:node_6995,prsc:2|A-3976-TTR,B-1493-OUT;n:type:ShaderForge.SFN_VertexColor,id:2886,x:32224,y:32771,varname:node_2886,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3360,x:31512,y:32621,varname:node_3360,prsc:2|A-6560-OUT,B-7476-T;n:type:ShaderForge.SFN_Multiply,id:765,x:31499,y:32799,varname:node_765,prsc:2|A-7476-T,B-2108-OUT;n:type:ShaderForge.SFN_Time,id:7476,x:31161,y:32662,varname:node_7476,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:2108,x:31202,y:32855,ptovrint:False,ptlb:node_2108,ptin:_node_2108,varname:node_2108,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:6560,x:31220,y:32572,ptovrint:False,ptlb:node_6560,ptin:_node_6560,varname:node_6560,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.8;n:type:ShaderForge.SFN_Slider,id:8510,x:31597,y:32811,ptovrint:False,ptlb:node_8510,ptin:_node_8510,varname:node_8510,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_TexCoord,id:380,x:31762,y:33079,varname:node_380,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:6905,x:32054,y:33081,varname:node_6905,prsc:2,spu:-1,spv:0|UVIN-380-UVOUT;n:type:ShaderForge.SFN_Panner,id:6015,x:32242,y:33105,varname:node_6015,prsc:2,spu:0,spv:0|UVIN-6905-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:4489,x:32410,y:33117,ptovrint:False,ptlb:node_4489,ptin:_node_4489,varname:node_4489,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:099c23d07f5f7264285510b50497c72c,ntxv:0,isnm:False|UVIN-6015-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:8915,x:31934,y:32907,varname:node_8915,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ComponentMask,id:2946,x:32138,y:32946,varname:node_2946,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-8915-UVOUT;n:type:ShaderForge.SFN_Multiply,id:3298,x:32410,y:32959,varname:node_3298,prsc:2|A-3890-OUT,B-2946-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3890,x:32265,y:32917,ptovrint:False,ptlb:node_3890,ptin:_node_3890,varname:node_3890,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:6328,x:32793,y:32964,varname:node_6328,prsc:2|A-3298-OUT,B-4489-R,C-798-OUT;proporder:6074-797-1493-2108-6560-8510-4489-3890;pass:END;sub:END;*/

Shader "Shader Forge/particle2_DISOVE" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        [HDR]_TintColor ("Color", Color) = (0.8113208,0.3865255,0.3865255,1)
        _time_frequency ("time_frequency", Range(0, 1)) = 0.3756581
        _node_2108 ("node_2108", Float ) = 0
        _node_6560 ("node_6560", Float ) = -0.8
        _node_8510 ("node_8510", Range(-1, 1)) = 1
        _node_4489 ("node_4489", 2D) = "white" {}
        _node_3890 ("node_3890", Float ) = 0.5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma target 2.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _node_4489; uniform float4 _node_4489_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _TintColor)
                UNITY_DEFINE_INSTANCED_PROP( float, _node_3890)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float _node_3890_var = UNITY_ACCESS_INSTANCED_PROP( Props, _node_3890 );
                float4 node_4607 = _Time;
                float2 node_6015 = ((i.uv0+node_4607.g*float2(-1,0))+node_4607.g*float2(0,0));
                float4 _node_4489_var = tex2D(_node_4489,TRANSFORM_TEX(node_6015, _node_4489));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                clip(((_node_3890_var*i.uv0.r)*_node_4489_var.r*(_MainTex_var.a*i.vertexColor.a)) - 0.5);
////// Lighting:
////// Emissive:
                float4 _TintColor_var = UNITY_ACCESS_INSTANCED_PROP( Props, _TintColor );
                float3 emissive = (_MainTex_var.rgb*_TintColor_var.rgb*i.vertexColor.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
