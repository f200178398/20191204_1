﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class ElevatorTri : MonoBehaviour
{
    public GameObject riseObj;
    public GameObject top;
    public float speed;
    bool rise;
    // Start is called before the first frame update
    void Start()
    {
        riseObj = GameObject.Find("-----------AllScene----------");
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        StartCoroutine(Rise());

    }
    IEnumerator  Rise()
    {
        if(rise == true)
        {
            yield return new WaitForSeconds(10);
            transform.position = Vector3.Lerp(transform.position, top.transform.position, speed * Time.deltaTime);
            
        }
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == ("Controller"))
        {
            rise = true;
            print("Rise");

        }
    }

}
