﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMain : MonoBehaviour {
    public Dropdown m_d;

    public Object m_HpBarPrefab;
    public Object m_FloatingTextPrefab;
    static private UIMain m_Instance;
    static public UIMain Instance()
    {
        return m_Instance;
    }

    public void LoadHpBar(Transform t)
    {
        GameObject go = Instantiate(m_HpBarPrefab) as GameObject;
        go.transform.parent = this.transform;
       // FloatingBar fb = go.GetComponent<FloatingBar>();
       // fb.SetFollower(t);
    }

    public void SpawnFloatingText(Vector3 vPos3D)
    {
        GameObject go = Instantiate(m_FloatingTextPrefab) as GameObject;
        go.transform.parent = this.transform;
        go.SendMessage("Spawn", vPos3D);
       // FloatingText fb = go.GetComponent<FloatingText>();
       // fb.Spawn(;
    }

    public void DragItem(Dragger d)
    {
        d.transform.position = Input.mousePosition;
    }

    private void Awake()
    {
        m_Instance = this;
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnToggleChanged(ToggleGroup tg)
    {
        foreach (Toggle t in tg.ActiveToggles())
        {
            Debug.Log(t.name + ":" + t.isOn);
        }
     //   tg.ActiveToggles
    }

    public void OnClick(Image m)
    {
        Debug.Log(m.name);
    }

    public void OnDropdown(Dropdown d)
    {
        int iv = d.value;
        Debug.Log(d.options[iv].text);
    }
}
