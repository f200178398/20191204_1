﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SA
{
    public class UI_EnemyHealth : MonoBehaviour
    {
        Image healthBar;
        Canvas healthCanvas;
        public Color color_1;
        public Color color_2;

        public AI_data data;
        //public EnemyTarget enemyTarget;
        // Start is called before the first frame update
        void Start()
        {
            
            healthBar = GetComponent<Image>();
            healthCanvas = transform.parent.gameObject.GetComponent<Canvas>(); //獲得上層物件
            data = gameObject.transform.parent.transform.parent.GetComponentInParent<AI_data>();
        }

        // Update is called once per frame
        void Update()
        {
            float hp = data.GetHealthRate();
            healthBar.fillAmount = hp;
            if(hp > 0.3 && hp < 0.6)
            {

               // print(healthBar.fillAmount);
                healthBar.color = color_1;
            }
             else if( hp <= 0.3)
            {
                healthBar.color = color_2;
            }
            //print(hp);
            if (hp < 0) //必須設定死亡後 寫條消失
            {
                healthCanvas.gameObject.SetActive(false);
            }
        }
    }

}
