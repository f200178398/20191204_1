﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
public class UI_HealthBar : MonoBehaviour
{
    public Image hpBar;
    //public float maxHp;
    public float currentHp;
    public FightSystem fightSystem;
    public onTriget onTriget;
    void Start()
    {
        hpBar = GetComponent<Image>();
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
        //onTriget = GameObject.FindGameObjectWithTag("Controller").GetComponent<OnTrigger>();
    }

    // Update is called once per frame
    void Update()
    {
        HpBar();
    }
    void HpBar()
    {
        
        float hp = fightSystem.GetHealthRate();
        hpBar.fillAmount = hp;
        
        currentHp = fightSystem.currentHp;

    }
}
