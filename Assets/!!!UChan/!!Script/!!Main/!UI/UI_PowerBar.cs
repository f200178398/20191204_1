﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
public class UI_PowerBar : MonoBehaviour
{
    public Image powerBar;
    public float currentPower;
    public FightSystem fightSystem;

    void Start()
    {
        powerBar = GetComponent<Image>();
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
    }

 
    void Update()
    {
        PowerBar();
    }
    void PowerBar()
    {
        float power = fightSystem.GetPowerRate();

        powerBar.fillAmount = power;

        currentPower = fightSystem.currentPower;
    }
}
