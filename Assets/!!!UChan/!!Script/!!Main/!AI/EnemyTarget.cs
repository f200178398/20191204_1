﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace SA
{
    /// <summary>
    /// 各種State的表單
    /// </summary>
    public class FSM : MonoBehaviour
    {

        private StateMachine _stateMachine;
        public GameObject Controller;
        public float distanceToPlayer;

        public float searchingRange = 8.0f;
        public float attackRange = 2.0f;
        public AI_data data;
        public bool attack;
        // Start is called before the first frame update
        void Start()
        {
            //初始化，並給初始狀態
            data = GetComponent<AI_data>();
            _stateMachine = new StateMachine(StateType.Idle, data);
            Controller = GameObject.Find("Controller");
            //登錄狀態變化的線
            _stateMachine.AddTransition(StateType.Idle, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.Idle, StateType.Attack, TransitionType.GoTo_Attack);
            _stateMachine.AddTransition(StateType.Chase, StateType.Attack, TransitionType.GoTo_Attack);
            _stateMachine.AddTransition(StateType.Chase, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Attack, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.Attack, StateType.Idle, TransitionType.GoTo_Idle);


            //跟Data座連接
            searchingRange = data.float_checkInSight;
            attackRange = data.float_AttackRange;
        }



        // Update is called once per frame
        void Update()
        {
            distanceToPlayer = Vector3.Distance(this.transform.position, Controller.transform.position);

            //Debug.Log("Distance = " + distanceToPlayer);
            if (distanceToPlayer < searchingRange)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase, data);
                //StartCoroutine(StopAction());
            }
            if (distanceToPlayer < attackRange)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_Attack, data);
                // StartCoroutine(StopAction());
            }


            //狀態機做事！
            _stateMachine.DoState(data);
        }


        //IEnumerator StopAction()
        //{
        //    Debug.Log("Stop Action now");
        //    yield return new WaitForSeconds(5);
        //}

        void OnDrawGizmos()
        {
            if (_stateMachine == null)
            {
                return;
            }
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 5.0f);

            if (_stateMachine.GetCurrentState == StateType.Idle)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(this.transform.position, searchingRange);
            }
            else if (_stateMachine.GetCurrentState == StateType.Chase)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(this.transform.position, searchingRange);
            }
            else if (_stateMachine.GetCurrentState == StateType.Attack)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(this.transform.position, attackRange);
            }

        }

    }








 




    /// <summary>
    /// EnemyTarget是掛在怪獸身上，呼叫執行State裡的東西的
    /// </summary>
    #region
    public class EnemyTarget : MonoBehaviour
    {
        public static EnemyTarget m_instance;
        public static EnemyTarget Instance()
        {
            return m_instance;
        }
        public EnemyTarget()
        {
            m_instance = this;
        }
        AI_data data;
        
        FightSystem fightSystem;
        WeaponTrigger weaponTrigger;
        private StateMachine _stateMachine;

        [Header("=====Enemy Type=====")]
        public bool b_IsKyle;
        public bool b_IsSpiderRobot;
        public bool b_IsLongLegRobot;
        public bool b_IsLastBoss;

        [Header("====我現在的狀態====")]
        public StateType currentState;

        bool Battack;
        private void Awake()
        {
            data = gameObject.GetComponent<AI_data>(); //從AI_data 要血量資料
            

            //aidata = GetComponent<AI_data>();
            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>(); //從戰鬥系統上要攻擊指令判定 和 主角傷害
            weaponTrigger = GameObject.Find("Weapon_point").GetComponent<WeaponTrigger>(); //從武器觸發系統上 檢視 有無觸發攻擊事件
        }

        void Start()
        {
            _stateMachine = new StateMachine(StateType.Idle, data);
            #region  三種雜魚的狀態串線
            _stateMachine.AddTransition(StateType.Idle, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.Chase, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Chase, StateType.Attack, TransitionType.GoTo_Attack);
            _stateMachine.AddTransition(StateType.Idle, StateType.Attack, TransitionType.GoTo_Attack);
            _stateMachine.AddTransition(StateType.Attack, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.Attack, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Attack, StateType.TakeDamage, TransitionType.GoTo_TakeDamage);
            _stateMachine.AddTransition(StateType.TakeDamage, StateType.Attack, TransitionType.GoTo_Attack);
            _stateMachine.AddTransition(StateType.Idle, StateType.TakeDamage, TransitionType.GoTo_TakeDamage);
            _stateMachine.AddTransition(StateType.TakeDamage, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Chase, StateType.TakeDamage, TransitionType.GoTo_TakeDamage);
            
            _stateMachine.AddTransition(StateType.TakeDamage, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.TakeDamage, StateType.Back, TransitionType.GoTo_Back);
            _stateMachine.AddTransition(StateType.Back, StateType.TakeDamage, TransitionType.GoTo_TakeDamage);
            _stateMachine.AddTransition(StateType.Back, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Idle, StateType.Back, TransitionType.GoTo_Back);
            _stateMachine.AddTransition(StateType.Chase, StateType.Back, TransitionType.GoTo_Back);
            _stateMachine.AddTransition(StateType.Back, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.Attack, StateType.Back, TransitionType.GoTo_Back);           
            _stateMachine.AddTransition(StateType.Back, StateType.Attack, TransitionType.GoTo_Attack);

            _stateMachine.AddTransition(StateType.Idle, StateType.Shoot, TransitionType.GoTo_Shoot);
            _stateMachine.AddTransition(StateType.Chase, StateType.Shoot, TransitionType.GoTo_Shoot);
            _stateMachine.AddTransition(StateType.Back, StateType.Shoot, TransitionType.GoTo_Shoot);
            _stateMachine.AddTransition(StateType.TakeDamage, StateType.Shoot, TransitionType.GoTo_Shoot);
            _stateMachine.AddTransition(StateType.Shoot, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Shoot, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.Shoot, StateType.Back, TransitionType.GoTo_Back);
            _stateMachine.AddTransition(StateType.Shoot, StateType.TakeDamage, TransitionType.GoTo_TakeDamage);

            _stateMachine.AddTransition(StateType.UnBack, StateType.TakeDamage, TransitionType.GoTo_TakeDamage);
            _stateMachine.AddTransition(StateType.UnBack, StateType.Shoot, TransitionType.GoTo_Shoot);
            _stateMachine.AddTransition(StateType.UnBack, StateType.Chase, TransitionType.GoTo_Chase);
            _stateMachine.AddTransition(StateType.UnBack, StateType.Attack, TransitionType.GoTo_Attack);
            _stateMachine.AddTransition(StateType.UnBack, StateType.Back, TransitionType.GoTo_Back);
            _stateMachine.AddTransition(StateType.UnBack, StateType.Idle, TransitionType.GoTo_Idle);
            _stateMachine.AddTransition(StateType.Idle, StateType.UnBack, TransitionType.GOTo_UnBack);
            _stateMachine.AddTransition(StateType.Attack, StateType.UnBack, TransitionType.GOTo_UnBack);
            _stateMachine.AddTransition(StateType.Chase, StateType.UnBack, TransitionType.GOTo_UnBack);
            _stateMachine.AddTransition(StateType.Shoot, StateType.UnBack, TransitionType.GOTo_UnBack);
            _stateMachine.AddTransition(StateType.Back, StateType.UnBack, TransitionType.GOTo_UnBack);
            _stateMachine.AddTransition(StateType.TakeDamage, StateType.UnBack, TransitionType.GOTo_UnBack);


            #endregion


            #region 大魔王狀態變化的串線
            //大魔王的狀態變化的線

            //普通追  攻擊1
            _stateMachine.AddTransition(StateType.LastBossChase, StateType.LastBossAttack1, TransitionType.GoTo_LastBossAttack1);
            _stateMachine.AddTransition(StateType.LastBossAttack1, StateType.LastBossChase, TransitionType.GoTo_LastBossChase);

            //普通追  攻擊2
            _stateMachine.AddTransition(StateType.LastBossChase, StateType.LastBossAttack2, TransitionType.GoTo_LastBossAttack2);
            _stateMachine.AddTransition(StateType.LastBossAttack2, StateType.LastBossChase, TransitionType.GoTo_LastBossChase);

            //普通追  射擊1
            _stateMachine.AddTransition(StateType.LastBossChase, StateType.LastBossShoot, TransitionType.GoTo_LastBossShoot);
            _stateMachine.AddTransition(StateType.LastBossShoot, StateType.LastBossChase, TransitionType.GoTo_LastBossChase);

            //第一次追 到攻擊    一方通行
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossFirstChase, TransitionType.GoTo_LastBossFirstChase);
            _stateMachine.AddTransition(StateType.LastBossFirstChase, StateType.LastBossAttack2, TransitionType.GoTo_LastBossAttack2);

            //Idle
            _stateMachine.AddTransition(StateType.LastBossAttack1, StateType.LastBossIdle, TransitionType.GoTo_LastBossIdle);
            _stateMachine.AddTransition(StateType.LastBossAttack2, StateType.LastBossIdle, TransitionType.GoTo_LastBossIdle);
            _stateMachine.AddTransition(StateType.LastBossShoot, StateType.LastBossIdle, TransitionType.GoTo_LastBossIdle);
            _stateMachine.AddTransition(StateType.LastBossChase, StateType.LastBossIdle, TransitionType.GoTo_LastBossIdle);
            _stateMachine.AddTransition(StateType.LastBossIdle, StateType.LastBossChase, TransitionType.GoTo_LastBossChase);
            _stateMachine.AddTransition(StateType.LastBossIdle, StateType.LastBossAttack1, TransitionType.GoTo_LastBossAttack1);
            _stateMachine.AddTransition(StateType.LastBossIdle, StateType.LastBossAttack2, TransitionType.GoTo_LastBossAttack2);
            _stateMachine.AddTransition(StateType.LastBossIdle, StateType.LastBossShoot, TransitionType.GoTo_LastBossShoot);


            //以下沒用到
            //普通追 去 跳
            _stateMachine.AddTransition(StateType.LastBossChase, StateType.LastBossJump, TransitionType.GoTo_LastBossJump);
            _stateMachine.AddTransition(StateType.LastBossJump, StateType.LastBossChase, TransitionType.GoTo_LastBossChase);

            //以下待整理

            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossShoot, TransitionType.GoTo_LastBossShoot);
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossChase, TransitionType.GoTo_LastBossChase);
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossIdle, TransitionType.GoTo_LastBossIdle);
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossAttack1, TransitionType.GoTo_LastBossAttack1);
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossAttack2, TransitionType.GoTo_LastBossAttack2);
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossFirstChase, TransitionType.GoTo_LastBossFirstChase);
            _stateMachine.AddTransition(StateType.Idle, StateType.Dead, TransitionType.GoTo_Dead);

            _stateMachine.AddTransition(StateType.LastBossShoot, StateType.Chase, TransitionType.GoTo_Chase);


            ////////////////////下面是新版/////////////////////

            //第一次Idle no.0
            _stateMachine.AddTransition(StateType.Idle, StateType.LastBossFirstIdle, TransitionType.GoTo_LastBossFirstIdle);

            //第一次shoot no.4
            _stateMachine.AddTransition(StateType.LastBossFirstIdle, StateType.LastBossFirstShoot, TransitionType.GoTo_LastBossFirstShoot);
            _stateMachine.AddTransition(StateType.LastBossFirstShoot, StateType.LastBossIdle1, TransitionType.GoTo_LastBossIdle1);

            //新Idle no.1
            _stateMachine.AddTransition(StateType.LastBossIdle1, StateType.LastBossIdle, TransitionType.GoTo_LastBossFirstIdle);//回去舊idle
            _stateMachine.AddTransition(StateType.LastBossIdle1, StateType.LastBossChase1, TransitionType.GoTo_LastBossChase1);//去追 no.2
            _stateMachine.AddTransition(StateType.LastBossIdle1, StateType.LastBossJump1, TransitionType.GoTo_LastBossJump1);//去跳 no.7
            _stateMachine.AddTransition(StateType.LastBossIdle1, StateType.LastBossAttackNew1, TransitionType.GoTo_LastBossAttackNew1);//去近戰 no.6
            _stateMachine.AddTransition(StateType.LastBossIdle1, StateType.LastBossShootBigBall, TransitionType.GoTo_LastBossShootBigBall);//去射大球 no.5

            //跳→只回Idle no.7
            _stateMachine.AddTransition(StateType.LastBossJump1, StateType.LastBossIdle1, TransitionType.GoTo_LastBossIdle1);

            //追，到各種攻擊 no.2
            _stateMachine.AddTransition(StateType.LastBossChase1, StateType.LastBossJump1, TransitionType.GoTo_LastBossJump1);
            _stateMachine.AddTransition(StateType.LastBossChase1, StateType.LastBossShootBigBall, TransitionType.GoTo_LastBossShootBigBall);
            _stateMachine.AddTransition(StateType.LastBossChase1, StateType.LastBossAttackNew1, TransitionType.GoTo_LastBossAttackNew1);
            _stateMachine.AddTransition(StateType.LastBossChase1, StateType.LastBossIdle1, TransitionType.GoTo_LastBossIdle1);//回到idle串安心的

            //近攻→只回到Idle no.6
            _stateMachine.AddTransition(StateType.LastBossAttackNew1, StateType.LastBossIdle1, TransitionType.GoTo_LastBossIdle1);

            //射大球→只回到Idle no.5
            _stateMachine.AddTransition(StateType.LastBossShootBigBall, StateType.LastBossIdle1, TransitionType.GoTo_LastBossIdle1);

            //死
            _stateMachine.AddTransition(StateType.LastBossFirstIdle, StateType.LastBossDeath, TransitionType.GoTo_LastBossDeath);
            _stateMachine.AddTransition(StateType.LastBossFirstShoot, StateType.LastBossDeath, TransitionType.GoTo_LastBossDeath);
            _stateMachine.AddTransition(StateType.LastBossChase1, StateType.LastBossDeath, TransitionType.GoTo_LastBossDeath);
            _stateMachine.AddTransition(StateType.LastBossAttackNew1, StateType.LastBossDeath, TransitionType.GoTo_LastBossDeath);
            _stateMachine.AddTransition(StateType.LastBossJump1, StateType.LastBossDeath, TransitionType.GoTo_LastBossDeath);
            _stateMachine.AddTransition(StateType.LastBossShootBigBall, StateType.LastBossDeath, TransitionType.GoTo_LastBossDeath);

            _stateMachine.AddTransition(StateType.LastBossDeath, StateType.LastBossIdle, TransitionType.GoTo_LastBossIdle);
            #endregion

        }

        // Update is called once per frame
        void Update()
        {

            if (b_IsKyle)
            {
                if (!AI_Function.CheckTargetEnemyInSight(data))  //OK
                {
                    print("Idle 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Idle, data);
                }
                else if (AI_Function.CheckTargetEnemyInSight(data)) // 警戒 到 Chase
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase, data);
                }
                if (AI_Function.ToChase(data))// Chase 到 Attack
                {
                    print("Attack 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Attack, data);

                }
                
                if(data.takeDamage == true)
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_TakeDamage, data);
                }

                
                AI_Function.Dead(data);

            }

            if (b_IsLongLegRobot)
            {
                if (!AI_Function.CheckTargetEnemyInSight(data))  //OK
                {
                   // print("Idle 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Idle, data);
                }
                else if (AI_Function.CheckTargetEnemyInSight(data))
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase, data);
                }
                if (AI_Function.ToChase(data))
                {
                    //print("Attack 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Attack, data);

                }
                if (data.takeDamage == true)
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_TakeDamage, data);
                }
                if (AI_Function.InBackRange(data))
                {
                    print("Back 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Back, data);
                }
                if (AI_Function.Distance(data) > data.backRang)
                {
                AI_Function.UnBack(data);

                }
                
                AI_Function.Dead(data);
            }

            if (b_IsSpiderRobot)  
            {
                if (!AI_Function.CheckTargetEnemyInSight(data))  //OK
                {
                    // print("Idle 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Idle, data);
                }
                else if (AI_Function.CheckTargetEnemyInSight(data))
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase, data);
                }
                if (AI_Function.Distance(data) < data.float_AttackRange)
                {
                    print("Shoot 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Shoot, data);

                }
                if (data.takeDamage == true)
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_TakeDamage, data);
                }
                if (AI_Function.InBackRange(data))
                {
                    //print("Back 狀態"); 
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Back, data); //目前後退的速度讓主角跟不上 是否需要計時器
                }
                if (AI_Function.Distance(data) > data.backRang)// 不做後退 
                {
                    AI_Function.UnBack(data);  
                    //_stateMachine.ExecuteTransition(TransitionType.GOTo_UnBack, data);// 加了這行後突然無法攻擊  也無法追擊 是否不需要這種狀態
                }

                AI_Function.Dead(data);

                //問題 : 無法在射擊時 停止WALK的動作
            }
           

            IsLastBoss(data);

            _stateMachine.DoState(data);
            currentState = _stateMachine.GetCurrentState;



            AI_Function.MachineDamage(data, fightSystem);
            
            //AI_Function.Dead(data);
        }

        protected void IsKyle(AI_data data)
        {

            if (b_IsKyle == false) { return; }
            {
                if (!AI_Function.CheckTargetEnemyInSight(data))  //OK
                {
                    print("Idle 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Idle, data);
                }
                else if (AI_Function.CheckTargetEnemyInSight(data))
                {
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase, data);
                }
                if (AI_Function.ToChase(data))
                {
                    print("Attack 狀態");
                    _stateMachine.ExecuteTransition(TransitionType.GoTo_Attack, data);

                }

                AI_Function.Dead(data);

            }
            //if (b_IsKyle == false) { return; }

            //if (!AI_Function.CheckTargetEnemyInSight(this.data))
            //{
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_Idle, this.data);
            //}
            //else if (AI_Function.CheckTargetEnemyInSight(this.data))
            //{
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase, this.data);
            //}
        }



        //else 
        //{
        //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossChase, data);
        //}
        protected void IsLastBoss(AI_data data)
        {
            if (b_IsLastBoss == false) { return; }
            //data.bubbleShooter.BubbleMakerRotate(data.go_LBEmitterHolder,0.0f, 0.0f, 1.0f);
            data.bubbleShooter.BubbleMakerRotate(data.go_LBAttackEmitterHolder, 0.0f, 0.0f, 1.0f);

            //AI_Function.LastBossCheckOnGround(data);//確認是否著地
            float distanceRangeSupport = Random.Range(0.0f, 1.0f);//距離測定用的輔助數值

            //死亡一定放在最上面第一次判斷
            if (data.currentHp < 0)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossDeath, data);
            }


            #region 第一版AI狀態變化

            ////第一版
            //if (data.i_LBStatePattern == 5)
            //{
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossIdle, data);
            //}
            //if (data.f_LBTimer >= data.f_LBTimerTemporarySave && data.i_LBStatePattern == 0)
            //{
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossFirstChase, data);
            //}
            //else if (data.f_LBTimer > data.f_LBTimerTemporarySave && data.i_LBStatePattern == 1)
            //{
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossChase, data);
            //}
            //else if (AI_Function.Distance(data) <= data.f_LBMelleAttackRange1 + distanceRangeSupport && data.i_LBStatePattern == 2)
            //{
            //    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange1;
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossAttack1, data);
            //}
            //else if (AI_Function.Distance(data) <= data.f_LBMelleAttackRange2 + distanceRangeSupport && data.i_LBStatePattern == 3)
            //{
            //    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange2;
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossAttack2, data);
            //}

            //else if ( AI_Function.Distance(data) <= data.f_LBMelleAttackRange1 + distanceRangeSupport && data.i_LBStatePattern == 3)
            //{
            //    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange1;
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossJump, data);
            //}
            //else if (AI_Function.Distance(data) <= data.f_LBShootAttackRange + distanceRangeSupport && data.i_LBStatePattern == 4)
            //{
            //    data.f_LBDeciedAttackRange = data.f_LBShootAttackRange;
            //    _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossShoot, data);
            //}

            #endregion

            #region 第二版AI狀態變化
            //第二版
            if (data.i_LBStatePattern == 0 && AI_Function.Distance(data) < 70.0f)//設定一個電梯平台到位的適當的距離，作為啟動魔王的點
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossFirstIdle, data);
            }
            else if (data.i_LBStatePattern == 4)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossFirstShoot, data);
            }
            else if (data.i_LBStatePattern == 1)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossIdle1, data);
            }
            else if (data.i_LBStatePattern == 7)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossJump1, data);
            }
            else if (data.i_LBStatePattern == 2)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossChase1, data);
            }
            else if (data.i_LBStatePattern == 6)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossAttackNew1, data);
            }
            else if (data.i_LBStatePattern == 5)
            {
                _stateMachine.ExecuteTransition(TransitionType.GoTo_LastBossShootBigBall, data);
            }
            #endregion
        }



        //_stateMachine.DoState(this.data);
        //currentState = _stateMachine.GetCurrentState;
    

    }
}
#endregion
