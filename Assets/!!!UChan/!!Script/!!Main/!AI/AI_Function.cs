﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class AI_Function : MonoBehaviour
{
    
    private void Start()
    {
        
    }


    public static GameObject CheckControllerInSight(AI_data data, ref bool bAttack)
    {
        GameObject controller = data.controller;
        Vector3 v = controller.transform.position - data.enemy.transform.position;
        float fDist = v.magnitude; //距離為向量長度
        if (fDist < data.float_AttackRange) //距離小於攻擊範圍時
        {
            //Debug.Log("進入敵人 :" + data.enemy.name + " 的攻擊範圍");
            bAttack = true;
            Debug.Log("進入敵人 :" + data.enemy.name + " 的攻擊範圍" + bAttack);
            return controller;
        }
        else if (fDist < data.float_checkInSight)
        {
            Debug.Log("進入敵人 :" + data.enemy.name + " 的警戒範圍" + bAttack);
            bAttack = false;
            return controller;
        }
        return null;
    }

    public static bool CheckTargetEnemyInSight(AI_data data)
    {
        GameObject enemy = data.enemy;
        Vector3 v = enemy.transform.position - data.controller.transform.position;
        float fDist = v.magnitude;
        if (fDist < data.float_AttackRange)
        {
            data.bool_Attack = true;
            print("敵人: " + enemy.name + "的攻擊狀態 : " + data.bool_Attack);
            return true;
        }
        else if (fDist < data.float_checkInSight)
        {
            data.bool_Attack = false;
            return true;
        }
        return false;
    }

    public static void Move(AI_data data)
    {
        if (data.bool_EnemyIsMoving == false)//如果移動狀態=假，不做這個Move的動作
        {
            return;
        }

        Transform thisEnemyTransfrom = data.enemy.transform;//這隻怪獸的位置一開始的Trasform，先存起來
        Vector3 thisEnemyPosition = data.enemy.transform.position;//這怪獸的位置，先存起來
        Vector3 thisEnemyRight = thisEnemyTransfrom.right;//這怪獸右方
        Vector3 thisEnemyForward = thisEnemyTransfrom.forward;//這怪獸前方，用來偵測障礙物用的，什麼都沒有變過的前方先存起來
        Vector3 thisEnemyAddForceForMove = data.vec_EnemyCurrentVector;//這怪獸要移動的力量

        //下面這一串讓轉向力量太大的話不超過怪獸轉向最大速度
        if (data.float_EnemyTempRotateForce > data.float_EnemyMaxRotateSpeed)
        {
            data.float_EnemyTempRotateForce = data.float_EnemyMaxRotateSpeed;
        }
        else if (data.float_EnemyTempRotateForce < -data.float_EnemyMaxRotateSpeed)
        {
            data.float_EnemyTempRotateForce = -data.float_EnemyMaxRotateSpeed;
        }

        //下面這一串才是移動，先算出要往哪邊走
        thisEnemyAddForceForMove = thisEnemyAddForceForMove + thisEnemyRight * data.float_EnemyTempRotateForce;//總力道，包刮往前和轉彎
        thisEnemyAddForceForMove.Normalize();//把力道Normolize
        thisEnemyTransfrom.forward = thisEnemyAddForceForMove;//把移動的力量指派給往前的向量

        //下面這一串決定移動的速度
        data.float_EnemyCurrentMoveSpeed = data.float_EnemyCurrentMoveSpeed + data.float_EnemyCurrentMoveForce * Time.deltaTime;//速度=力道*Time
        if (data.float_EnemyCurrentMoveSpeed < 0.01f)//如果速度太小的話，讓他還是會移動，不要太小小到一直在計算
        {
            data.float_EnemyCurrentMoveSpeed = 0.01f;
        }
        else if (data.float_EnemyCurrentMoveSpeed > data.float_EnemyMaxSpeed)//如果超過怪獸最大速度，讓他等於最大速不要超過的意思
        {
            data.float_EnemyCurrentMoveSpeed = data.float_EnemyMaxSpeed;
        }

        //check障礙物先跳過

        //下面是算出位置(終於要移動的意思)
        thisEnemyPosition = thisEnemyPosition + thisEnemyTransfrom.forward * data.float_EnemyCurrentMoveSpeed;//位移=現在位置+往前的向量(已存Normalized後的力)
        thisEnemyTransfrom.position = thisEnemyPosition;//把新的位置告訴怪獸的Transform
    }


    public static void Shoot(AI_data data)
    {
        //是否具有遠距攻擊屬性，沒的話就掰
        if (data.bool_EnemyHasRemoteAttackProperty == false)
        {
            data.transArray_Emiiter = null;//如果沒有射擊屬性，發射口也給空
            return;
        }

        //把AI_data的泡泡射擊速度 傳到 BubbleShoot_3 裡，再把它存起來
        data.bubbleShooter.resetShootingTime = data.float_BubbleShootSpeed;
        float f_BubbleShootSpeed = data.bubbleShooter.resetShootingTime;
        //同上，第一次射擊前的延遲時間
        data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
        float f_FirstShootDelayTime = data.bubbleShooter.firstShootingCountDown;
        //同上，一波泡泡攻擊時間
        data.bubbleShooter.totalWaveShootingTime = data.float_WaveShootTotalTime;
        float f_WaveShootTotalTime = data.bubbleShooter.totalWaveShootingTime;
        //同上，整波的射完之後的延遲時間
        data.bubbleShooter.rechargeTime = data.float_WaveShootOverDelayTime;
        float f_WaveShootOverDelayTime = data.bubbleShooter.rechargeTime;
        //同上，泡泡的發射口(注意是一個陣列)
        data.bubbleShooter.bubbleEmitter = data.transArray_Emiiter;
        Transform[] trans_array_Emitter = data.bubbleShooter.bubbleEmitter;

        data.anim.SetBool("chase", false); //射擊時候CHASE 試試看能否寫在狀態機

        //Debug.Log("AIFunction");

        data.bubbleShooter.BubbleShooting();// 開啟後開始發射



    }
    public static bool Idle(AI_data data)
    {
        //data.anim.SetBool("chase", false);
        data.anim.SetTrigger("UndamageTri");
        data.anim.SetBool("take damage", false);
        return true;//回傳的True 或 False 決定了在其他函示裡一 開始的職
    }



    public static float Distance(AI_data data)
    {
        GameObject controller = data.controller;
        GameObject enemy = data.enemy;

        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);

        return distance;
    }

    public static bool CheckInSight(AI_data data) //檢查是否視距內
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;
        Vector3 v = data.dirToPlayer;
        GameObject controller = data.controller;
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        float angle = Mathf.Atan2(data.dirToPlayer.x, data.dirToPlayer.z);
        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);
        bool insight = true;
        if (distance < data.float_checkInSight)
        {

            if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
            {

                Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
                print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
                data.dirToPlayer += hit.normal * 30.0f;

            }
            else
            {
                // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
                Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);

                //dirToPlayer = dirToPlayer;
            }

            //Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
            //enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
            //data.movementVec = enemy.transform.forward * data.Chease_speed;

            //data.Chease_speed = 0.2f;
            //data.anim.SetBool("chase", true);
            //data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
            //data.isChase = true;
        }

        //bool insight;
        ////print(Distance(data));
        //if (Distance(data) < data.float_checkInSight)
        //{

        //    insight = true;
        //    print(data.enemy.name + "CheckInSight :"  +insight);
        //    //Chase(data);
        //    return insight;
        //}
        //else
        //{
        //    insight = false;
        //    return insight;
        //}
        return insight;
    }
    public static bool SlowDown(AI_data data) //檢查是否在慢速區內
    {
        bool insight;
        if (Distance(data) < data.float_SlowDownDistance)
        {
            insight = true;
            print(data.enemy.name + "SlowDown :" + insight);
        }
        return true;
    }

    public static bool InAttackRange(AI_data data) //檢查是否在攻擊區內
    {
        
        print("InAttackRange");
        if (Distance(data) < data.float_AttackRange)
        {
            //data.Chease_speed = 0f;
            data.anim.SetBool("chase", false);
            data.bool_InAttackRange = true;
            data.isChase = false;

        }
        else if (Distance(data) < data.float_AttackRange && data.bool_InAttackRange == true)
        {
            data.float_nextChase += Time.deltaTime;
            print(" 超出攻擊範圍");
            data.anim.SetBool("chase", false);
            data.Chease_speed = 0f;
            if (data.float_nextChase > data.float_chaseRate && Distance(data) < data.float_AttackRange)   //會在主角離開攻擊範圍後數秒再次追擊
            {
                data.anim.SetBool("chase", true);
                data.isChase = true;
                data.Chease_speed = 0.1f;


                data.bool_InAttackRange = false;

                data.float_nextChase = 0;
            }

            //bool insight;
            //if (Distance(data) < data.float_AttackRange)
            //{
            //    insight = true;
            //    data.anim.SetBool("chase", false);
            //    print(data.enemy.name + "InAttackRange :" + insight);
            //}

        }
        //else
        //{
        //    data.boo1_InAttackRange = false;
        //}
        return true;
    }

    public static bool InBackRange(AI_data data)
    {
        
        if (Distance(data) > data.backRang)//再後退範圍內
        {

            data.inBackRange = false;
        }
        else
        {
            data.inBackRange = true;
        }
        return data.inBackRange;
    }
    public static void ToBack(AI_data data)
    {
        GameObject controller = data.controller;
        GameObject enemy = data.enemy;
        float distance = Vector3.Distance(data.enemy.transform.position, data.controller.transform.position);
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        if (data.inBackRange == true)//再後退範圍內
        {
            data.backTime += Time.deltaTime;
            //data.Chease_speed = -0.05f;

            //data.anim.SetBool("Back", true);
            data.isChase = false;

            if (data.backTime > data.backRate && Distance(data) < data.backRang)   //在主角進入後退範圍後的一小段後, 執行後退
            {
                data.backSpeed = -0.1f;
                data.anim.SetBool("Back", true);
                data.isChase = false;
                


                data.bool_InAttackRange = false;

                data.backTime = 0;
            }
            Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
            enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
            data.movementVec = enemy.transform.forward * data.backSpeed;

            data.movementVec = enemy.transform.forward * -0.1f;
            data.anim.SetBool("Back", true);
            data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
            data.isBack = true;
        }
        if (data.inBackRange == false)
        {
            data.anim.SetBool("Back", false);
        }
    }
    public static void UnBack(AI_data data)
    {
        //if(Distance(data) > data.backRang)
        //{
            data.backTime = 0;
            data.backSpeed = 0;
            data.anim.SetBool("Back", false);
            data.isBack = false;
        //} 

            
            
    }
    public static void Back(AI_data data)
    {
        GameObject controller = data.controller;
        GameObject enemy = data.enemy;
        float distance = Vector3.Distance(data.enemy.transform.position, data.controller.transform.position);
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;
        if (distance < data.backRang)//再後退範圍內
        {
            data.backTime += Time.deltaTime;
            //data.Chease_speed = -0.05f;

            //data.anim.SetBool("Back", true);
            data.isChase = false;

            if (data.backTime > data.backRate && distance < data.backRang)   //在主角進入後退範圍後的一小段後, 執行後退
            {
                data.anim.SetBool("Back", true);
                data.isChase = false;
                data.backSpeed = -0.02f;


                data.bool_InAttackRange = false;

                data.backTime = 0;
            }
            Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
            enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
            //data.movementVec = enemy.transform.forward * data.backSpeed;

            data.movementVec = enemy.transform.forward *-1f;
            data.anim.SetBool("Back", true);
            data.rb.MovePosition(data.rb.transform.position + data.movementVec * 0.1f);
            data.isBack = true;
        }
        
       
    }
    

    public static bool ToChase(AI_data data)
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;
        Vector3 v = data.dirToPlayer;
        GameObject controller = data.controller;
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        float angle = Mathf.Atan2(data.dirToPlayer.x, data.dirToPlayer.z);
        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);
        if (!Dead(data))
        {
            if (distance > data.float_checkInSight)
            {
                data.anim.SetBool("chase", false);
                data.isChase = false;
            }

            if (distance < data.float_checkInSight)
            {

                if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
                {

                    Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
                    print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
                    data.dirToPlayer += hit.normal * 30.0f;

                }
                else
                {
                    // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
                    Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);
                    //dirToPlayer = dirToPlayer;
                }

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chease_speed;
                 //////
                float speed = data.Chease_speed;
                data.anim.SetBool("chase", true);
                data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
                data.isChase = true;
            }

            if (distance > data.float_AttackRange && distance <= data.float_SlowDownDistance)
            {

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chease_speed;


            }

            if (distance < data.float_AttackRange)  // 當距離小於攻擊範圍 速度為0  開始攻擊 並且 當離開攻擊範圍後  開始計時 並且兩秒後 才開始追擊
            {
                data.Chease_speed = 0f;
                data.anim.SetBool("chase", false);
                data.bool_InAttackRange = true;
                data.isChase = false;

            }
            else if (distance > data.float_AttackRange && data.bool_InAttackRange == true)
            {
                data.float_nextChase += Time.deltaTime;
                print(" 超出攻擊範圍");
                data.anim.SetBool("chase", false);
                data.Chease_speed = 0f;
                if (data.float_nextChase > data.float_chaseRate && distance > data.float_AttackRange)   //會在主角離開攻擊範圍後數秒再次追擊
                {
                    data.anim.SetBool("chase", true);
                    data.isChase = true;
                    data.Chease_speed = 0.1f;


                    data.bool_InAttackRange = false;

                    data.float_nextChase = 0;
                }


            }

        }

        return true;

    }

    public static void Chase(AI_data data) //備用
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;
        Vector3 v = data.dirToPlayer;
        GameObject controller = data.controller;
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        float angle = Mathf.Atan2(data.dirToPlayer.x, data.dirToPlayer.z);
        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);
        if (!Dead(data))
        {
            if (distance > data.float_checkInSight)
            {
                data.anim.SetBool("chase", false);
                data.isChase = false;
            }

            if (distance < data.float_checkInSight)
            {

                if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
                {

                    Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
                    print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
                    data.dirToPlayer += hit.normal * 30.0f;

                }
                else
                {
                    // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
                    Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);
                    //dirToPlayer = dirToPlayer;
                }

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chease_speed;

                float speed = data.Chease_speed;
                data.anim.SetBool("chase", true);
                data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
                data.isChase = true;
            }

            if (distance > data.float_AttackRange && distance <= data.float_SlowDownDistance)
            {

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chease_speed;


            }

            if (distance < data.float_AttackRange)  // 當距離小於攻擊範圍 速度為0  開始攻擊 並且 當離開攻擊範圍後  開始計時 並且兩秒後 才開始追擊
            {
                data.Chease_speed = 0f;
                data.anim.SetBool("chase", false);
                data.bool_InAttackRange = true;
                data.isChase = false;

            }
            else if (distance > data.float_AttackRange && data.bool_InAttackRange == true)
            {
                data.float_nextChase += Time.deltaTime;
                //print(" 超出攻擊範圍");
                data.anim.SetBool("chase", false);
                data.Chease_speed = 0f;
                if (data.float_nextChase > data.float_chaseRate && distance > data.float_AttackRange)   //會在主角離開攻擊範圍後數秒再次追擊
                {
                    data.anim.SetBool("chase", true);
                    data.isChase = true;
                    data.Chease_speed = 0.1f;


                    data.bool_InAttackRange = false;

                    data.float_nextChase = 0;
                }


            }

        }



    }
    public static void OpenShield(AI_data data) ///防護盾用
    {

        GameObject Shield = data.Shield;
        GameObject enemy = data.enemy;
        if(data.HaveShield == true)
        {

       
        if (Distance(data) <= data.shieldRadius)//小於攻擊距離
        {

            if (enemy.GetComponent<AI_data>().shieldPower == 0)//防護盾被打爆==0時
            {
                data.shieldOpen = false;
                Shield.gameObject.SetActive(false);
                //  Box.transform.localScale =Box.transform.localScale+new Vector3(Time.deltaTime,1,1);
                enemy.GetComponent<Rigidbody>().isKinematic = true;
                // print("泡泡破掉了");
            }
            
            else//沒被打爆靠近防護盾，防護盾起作用
            {
                data.shieldOpen = true;
                //print("start shield");
                Shield.gameObject.SetActive(true);
                Shield.transform.localScale = Shield.transform.localScale + new Vector3(1f, 1f, 1f);

                enemy.GetComponent<Rigidbody>().isKinematic = true;
            }
            
        }
       
        }
    }
    public static void ColseShield(AI_data data)
    {
        GameObject Shield = data.Shield;
        GameObject enemy = data.enemy;
        if(data.HaveShield == true)
        {
            if (Distance(data) > data.float_AttackRange  )//當主角跑離怪的偵查範圍
            {
                Shield.transform.localScale = Shield.transform.localScale - new Vector3(0.2f, 0.2f, 0.2f); //防護盾慢慢變小
                if (Shield.transform.localScale.x < 0)//防護盾不小於0縮放值
                {
                    Shield.transform.localScale = new Vector3(0, 0, 0);
                }
                //Box.gameObject.SetActive(false);
                enemy.GetComponent<Rigidbody>().isKinematic = false;

            }
        }
    }


    public static bool MelleAttack(AI_data data)
    {

        Animator animator = data.anim;
        bool isAttack = false;


        //data.enemyTarget.enemyDamage_Melle = data.int_MelleAttackD_Damage; //近戰攻擊力
        Transform thisEnemyTransfrom = data.enemy.transform;//這隻怪獸的位置一開始的Trasform，先存起來
        Vector3 thisEnemyPosition = data.enemy.transform.position;//這怪獸的位置，先存起來


        Vector3 controller = FightSystem.m_Instance.GetController().transform.position;// 找到主角
        bool startAttack = false;
        float attackRange = data.float_AttackRange;
        float attackRate = data.float_MelleAttackRate;
        float disForBoth = Vector3.Distance(controller, thisEnemyPosition);
        float nextAttack = data.float_NextMelleAttack;
        string[] attackAnim = data.MelleAttackAnim;
        string animation = null;
        if (data.bool_EnemyHasMelleAttackProperty == true)//如果是近戰
        {

            int attackAmin = UnityEngine.Random.Range(0, data.MelleAttackAnim.Length);
            if (CheckControllerInSight(data, ref isAttack) && data.takeDamage == false && data.isChase == false)
            {

                if (NextTimeAttack(data, ref startAttack) && data.isDead == false)
                {
                    animation = data.MelleAttackAnim[attackAmin];
                    //animator.SetBool("attack_01", isAttack);
                    isAttack = true;
                    data.anim.SetTrigger("UndamageTri");
                    print(data.enemy.name + "開始攻擊 :" + isAttack);
                    animator.CrossFade(animation, 0.0001f);
                }
                else
                {
                    animator.SetBool("attack_01", false);
                    data.anim.SetTrigger("UndamageTri");
                }

                // animation = data.MelleAttackAnim[attackAmin]; // 隨機攻擊動畫  但因為同時讀條 而讓動畫卡住



                //animator.CrossFade(animation, 0.0001f);
            }
            //else 
            //{

            //    animator.SetBool("attack_01", false);
            //    return;
            //}

        }

        return true;

    }

    public static void LookAtController(AI_data data) //未完成
    {

        Vector3 v = data.controller.transform.position - data.enemy.transform.position;
        float fDist = v.magnitude;
        if (fDist < data.float_checkInSight && !Dead(data))
        {
            float _lookRange = data.float_checkInSight + 5f;
            Quaternion rotation = Quaternion.LookRotation(data.controller.transform.position - data.enemy.transform.position, Vector3.up);
            data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, rotation, Time.deltaTime * data.float_LookRotationSpeed);

        }


    }

    public static void TakeDamage(AI_data data) //受傷
    {
        
        if (data.takeDamage == true)
        {
            
            
            //data.anim.SetTrigger("take damageTri");
           

        }
        
        if (data.takeDamage == false)
        {
           // data.anim.SetBool("take damage", false);
            //data.anim.SetTrigger("UndamageTri");
        }
    }

    public static void MachineDamage(AI_data data, FightSystem fightSystem) //護衛機的傷害
    {
        if (data.machineDamage == true)
        {

            print("敵人當前血量 : " + data.currentHp);
            data.anim.SetTrigger("take damageTri");
        }
        if (data.machineDamage == false)
        {
            data.anim.SetBool("take damage", false);
        }


    }

    public static bool Dead(AI_data data)
    {

        if (data.currentHp <= 0)
        {

            data.currentHp = 0;
            print("dead!");
            data.anim.SetBool("dead", true);
            Destroy(data.gameObject, 20f);
            data.isDead = true;

            //data.enabled = false;
        }

        return data.isDead;

    }

    public static void FirstTimeShoot(AI_data data)

    {
        if (data.IsFirstTimeShoot)
        {
            data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
            data.IsFirstTimeShoot = false;
        }
        else
        {
            data.bubbleShooter.firstShootingCountDown -= Time.deltaTime;
            //Debug.Log("First Attack Preparing" + data.bubbleShooter.firstShootingCountDown);
            if (data.bubbleShooter.firstShootingCountDown < 0)
            {


                return;
                data.bubbleShooter.firstShootingCountDown = 0;
                print(data.bubbleShooter.firstShootingCountDown);
            }
        }

    }

    //public static bool NextTimeChase(AI_data data ,ref bool _startChase)
    //{

    //    data.float_nextChase += Time.deltaTime;
    //    if (data.float_nextChase > data.float_chaseRate)
    //    {
    //        data.float_nextChase = 0;
    //        return true;
    //    }
    //    else
    //    {

    //        return false;
    //    }
    //}

    public static bool NextTimeAttack(AI_data data, ref bool _startAttack) //攻擊間隔
    {

        data.float_NextMelleAttack += Time.deltaTime;
        print("開始倒數 : " + data.float_FirstAttackCountDown);
        if (data.float_NextMelleAttack > data.float_MelleAttackRate) //下次攻擊倒數 
        {
            data.float_NextMelleAttack = 0;
            return true;
        }
        else
        {
            _startAttack = false;
            return false;
        }

    }

    //public void FirstTimeShoot(AI_States data)
    //{
    //    if (data.)
    //    {
    //        data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
    //        data.isfi = false;
    //    }
    //    else
    //    {
    //        data.bubbleShooter.firstShootingCountDown -= Time.deltaTime;
    //        Debug.Log("First Attack Preparing" + data.bubbleShooter.firstShootingCountDown);
    //    }
    //    if (data.bubbleShooter.firstShootingCountDown < 0)
    //    {
    //        data.bubbleShooter.firstShootingCountDown = 0;
    //        print("rest");
    //    }

    //}

    public static void TurrentRotation(AI_data data)
    {
        Quaternion rotation = Quaternion.LookRotation(data.controller.transform.position - data.enemy.transform.position, Vector3.up);
        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, rotation, Time.deltaTime * data.float_RotationSpeed);
    }




    #region 大魔王用的function

    public static float LastBossTimer(AI_data data)
    {
        data.f_LBTimer += Time.deltaTime;
        return data.f_LBTimer;
    }

    public static void LastBossRotate(AI_data data)//大魔王轉向
    {
        //轉向
        if (data.b_LBRotating == false) { return; }

        data.dirToPlayer = new Vector3(data.controller.transform.position.x - data.enemy.transform.position.x, 0.0f, data.controller.transform.position.z - data.enemy.transform.position.z);
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);




        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation.normalized, lookRotation, data.f_LBRotationSpeed);
    }

    /// <summary>
    /// float attackRange ，是走到離主角多少距離的地方
    /// </summary>
    public static void LastBossMoveForward(AI_data data, float attackRange)//大魔王移動
    {
        if (data.b_LBMoving == false) { return; }
        GameObject enemy = data.enemy;
        GameObject controller = data.controller;
        //下面是算距離的向量
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z));

        data.v_LBVectorChase = enemy.transform.forward * data.f_LBMoveSpeed;//有乘上速度的向量

        if (Vector3.Magnitude(data.dirToPlayer) > attackRange)
        {
            data.rb_LBrb.MovePosition(enemy.transform.position + data.v_LBVectorChase * 0.8f);//關鍵的移動的這一行
        }
        else
        {

        }


        // var LbMove = Vector3.Lerp(enemy.transform.position, enemy.transform.forward, 0.5f);
    }

    public static void LastBossMoveBackward(AI_data data)//大魔王移動
    {
        if (data.b_LBMoving == false) { return; }
        GameObject enemy = data.enemy;
        GameObject controller = data.controller;
        //下面是算距離的向量
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z));
        data.v_LBVectorChase = enemy.transform.forward * data.f_LBMoveSpeed;//有乘上速度的向量
        if (Vector3.Magnitude(data.dirToPlayer) < 10)
        {
            data.rb_LBrb.MovePosition(enemy.transform.position - data.v_LBVectorChase * 0.8f);
        }
        else
        {

        }
        // var LbMove = Vector3.Lerp(enemy.transform.position, enemy.transform.forward, 0.5f);
    }

    public static void LastBossAvoidObstacle(AI_data data)//大魔王避開牆，抄少彥的
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;

        if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
        {

            Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
            print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
            data.dirToPlayer += hit.normal * 30.0f;

        }
        else
        {
            // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
            Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);
            //dirToPlayer = dirToPlayer;
        }

    }


    public static void LastBossShoot(AI_data data, Transform[] emitters)
    {
        //是否具有遠距攻擊屬性，沒的話就掰
        if (data.bool_EnemyHasRemoteAttackProperty == false)
        {
            data.transArray_Emiiter = null;//如果沒有射擊屬性，發射口也給空
            return;
        }

        //把AI_data的泡泡射擊速度 傳到 BubbleShoot_3 裡，再把它存起來
        data.bubbleShooter.resetShootingTime = data.float_BubbleShootSpeed;
        float f_BubbleShootSpeed = data.bubbleShooter.resetShootingTime;
        //同上，第一次射擊前的延遲時間
        data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
        float f_FirstShootDelayTime = data.bubbleShooter.firstShootingCountDown;
        //同上，一波泡泡攻擊時間
        data.bubbleShooter.totalWaveShootingTime = data.float_WaveShootTotalTime;
        float f_WaveShootTotalTime = data.bubbleShooter.totalWaveShootingTime;
        //同上，整波的射完之後的延遲時間
        data.bubbleShooter.rechargeTime = data.float_WaveShootOverDelayTime;
        float f_WaveShootOverDelayTime = data.bubbleShooter.rechargeTime;
        //同上，泡泡的發射口(注意是一個陣列)
        data.bubbleShooter.bubbleEmitter = data.transArray_Emiiter;
        emitters = data.bubbleShooter.bubbleEmitter;



        //Debug.Log("AIFunction");

        data.bubbleShooter.BubbleShooting();
    }

    public static void LastBossCheckOnGround(AI_data data)
    {
        RaycastHit hit;
        if (Physics.Raycast(data.enemy.transform.position, -data.enemy.transform.up, out hit, 2.0f))
        {
            data.enemy.transform.position = new Vector3(data.enemy.transform.position.x, hit.point.y, data.enemy.transform.position.z);
        }
    }

    public static void LastBossShootEmitterRotate(AI_data data)
    {
        if (data.b_LBEmitterRotate == false) { return; }
        data.bubbleShooter.BubbleMakerRotate(data.go_LBEmitterHolder, 0, 0, 30f);

    }
    #endregion


}








