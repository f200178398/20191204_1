﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace SA
{
    /// <summary>
    /// 設定無論何種敵人的狀態機 例如: 近戰判定 , 遠程攻擊判定 ,追擊判定
    /// 不需要設定數值
    /// </summary>
    //public class AI_States : MonoBehaviour
    //{



    //    public GameObject UChan;
    //    public AI_data data;
    //    Animator anim;
    //    //string animation;

    //   [Header("偵測當前狀態")]
    //    public bool isMelleAttack; //是否為近戰攻擊中
    //    public bool isChase; //是否為追擊狀態中
    //    public bool isSeek; //是否為追蹤敵人中
    //    public float distance;
        
    //    public string[] MelleSkillAnim;
    //    void Start()
    //    {

    //        anim = GetComponent<Animator>();
    //        data = GetComponent<AI_data>(); 
    //        UChan = GameObject.Find("Controller");
    //        //anim = GetComponent<Animator>();
    //        StartCoroutine(AttackTime());
    //    }

    //    // Update is called once per frame
    //    void Update()
    //    {
            
            
            
    //    }
    //    public void DistanceOfTarget() // 計算與主角的距離
    //    {
    //        distance = Vector3.Distance(this.transform.position, UChan.transform.position);
    //    }
    //    public void FindTarget() //找到主角的相關動作
    //    {
            
    //    }

    //    public void MelleAttack() // 普通攻擊
    //    {
            
    //        string animation = null;
    //        int attack = Random.Range(0, MelleSkillAnim.Length);
    //        if (distance <= data.float_AttackRange)
    //        {
    //            animation = MelleSkillAnim[attack]; //隨機普通近戰攻擊
    //            isMelleAttack = true;
    //            print(animation);
    //        }
    //        else
    //        {
    //            isMelleAttack = false;
    //        }
    //        anim.CrossFade(animation, 0.1f);
    //    }
    //    public void CountTime()
    //    {
    //       //float StartCount += Time.deltaTime; 
    //    }
    //    IEnumerator AttackTime()
    //    {
    //        //for ( float i =0; i<= 5; i += Time.deltaTime )
    //        //{
    //        //    print( i + "second");
    //        //    yield return new WaitForSeconds(3); 
    //        //}
            
    //            print("second");
    //            yield return new WaitForSeconds(3);
            
    //    }
    //}

    #region 把State和Transition拿來用的class
    public class StateMachine
    {
        private StateType _stateType;
        private State _state;

        //把狀態的名稱和狀態class存在一起(配對完成)
        private Dictionary<StateType, State> _stateTouroku = new Dictionary<StateType, State>();
        //把狀態之間變換的條件給串好
        private Dictionary<StateType, List<Transition>> _transitionList = new Dictionary<StateType, List<Transition>>();

        //變更狀態，要離開這個狀態了
        public void ChangeState(StateType stateType, AI_data data)
        {
            if (_state != null)//防呆，不是空的狀態才可以離開
            {
                _state.ExitState(data);
            }

            _stateType = stateType;//把()裡的狀態丟給現狀態，成為新的狀態，這邊只改enum而已
            _state = _stateTouroku[_stateType];//這裡利用Dictionary串連enum的真state，丟給現狀態，變成新狀態
            _state.EnterState(data);//(因為狀態已經改變了)做狀態的Enter

        }

        //在狀態時一直做什麼
        public void DoState(AI_data data)
        {
            _state.OnState(data);
        }

        //取得現在的狀態
        public StateType GetCurrentState
        {
            get { return _stateType; }
        }

        //初始化
        public StateMachine(StateType initialState, AI_data data)
        {
            _stateTouroku.Add(StateType.Idle, new State_Idle());
            _stateTouroku.Add(StateType.Chase, new State_Chase());
            _stateTouroku.Add(StateType.Attack, new State_Attack());
            _stateTouroku.Add(StateType.TakeDamage, new State_TakeDamage());
            _stateTouroku.Add(StateType.Back, new State_Back());
            _stateTouroku.Add(StateType.Shoot, new State_Shoot());
            _stateTouroku.Add(StateType.UnBack, new State_UnBack());

            _stateTouroku.Add(StateType.LastBossShoot, new State_LastBossShoot());
            _stateTouroku.Add(StateType.LastBossFirstChase, new State_LastBossFirstChase());
            _stateTouroku.Add(StateType.LastBossChase, new State_LastBossChase());
            _stateTouroku.Add(StateType.LastBossAttack1, new State_LastBossMelleAttack1());
            _stateTouroku.Add(StateType.LastBossAttack2, new State_LastBossMelleAttack2());
            _stateTouroku.Add(StateType.LastBossJump, new State_LastBossJumpAttack());
            _stateTouroku.Add(StateType.LastBossMoveBack, new State_LastBossMoveBackward());
            _stateTouroku.Add(StateType.LastBossIdle, new State_LastBossIdle());

            _stateTouroku.Add(StateType.LastBossFirstIdle, new State_LastBossFirstIdle());//no.0
            _stateTouroku.Add(StateType.LastBossFirstShoot, new State_LastBossFirstShoot());//no.4
            _stateTouroku.Add(StateType.LastBossIdle1, new State_LastBossIdle1());//no.1
            _stateTouroku.Add(StateType.LastBossJump1, new State_LastBossJump1());//no.7
            _stateTouroku.Add(StateType.LastBossChase1, new State_LastBossChase1());//no.2
            _stateTouroku.Add(StateType.LastBossAttackNew1, new State_LastBossAttack1());//no.6
            _stateTouroku.Add(StateType.LastBossShootBigBall, new State_LastBossShootBigBall());//no.5
            _stateTouroku.Add(StateType.LastBossDeath, new State_LastBossDeath());//no.9
            ChangeState(initialState, data);//一開始就做什麼狀態
        }


        //增加變化條件(串起來)用的，但是裡面有很炫的LambDa語法我也不是很懂還要再研究
        public void AddTransition(StateType from, StateType to, TransitionType transitionType)
        {
            //如果串Goto的Dictionary裡沒有 from(原狀態)的話
            if (!_transitionList.ContainsKey(from))
            {
                _transitionList.Add(from, new List<Transition>());//就給他加一個(把他加到Dictionary裡)
            }
            List<Transition> addTransitions = _transitionList[from]; //把資料加到List裡(上面只有加到Dictionary而已)
            Transition tempTransition = addTransitions.FirstOrDefault(x => x.goToThisState == to);//抄來的很炫的Lambda
            if (tempTransition == null)//如果上面那個是空的，就進行新登錄
            {
                addTransitions.Add(new Transition { goToThisState = to, transitionType = transitionType });//把要變的條件加進去
            }
            else//如果已經有的話，就給他串成新的
            {
                tempTransition.goToThisState = to;//要去哪一個狀態
                tempTransition.transitionType = transitionType;//條件
            }
        }

        //******這個還要再研究
        public void ExecuteTransition(TransitionType transitionType, AI_data data)//進行變化狀態的動作
        {
            List<Transition> soManyTransitions = _transitionList[_stateType];//加到Dictionary的List把他先暫存起來(取出來的意思)
            foreach (Transition everyTransition in soManyTransitions)//List裡面每個東西都要做
            {
                if (everyTransition.transitionType == transitionType)//如果要去下個狀態 和 丟進來的一樣
                {
                    ChangeState(everyTransition.goToThisState, data);//做變化狀態
                    break;
                }
            }
        }


    }
    #endregion




    #region 各種State(包含母體)

    public enum StateType
    {
        Idle,
        Chase,
        Attack,
        TakeDamage,
        Dead,
        Back,
        Shoot,
        UnBack,
        /// <summary>
        /// 舊近1，砍一下，要改射
        /// </summary>
        LastBossAttack1,
        /// <summary>
        /// 舊近2，雙擊
        /// </summary>
        LastBossAttack2,
        /// <summary>
        /// 舊射
        /// </summary>
        LastBossShoot,
        /// <summary>
        /// 舊跳，沒用到
        /// </summary>
        LastBossJump,
        /// <summary>
        /// 舊追
        /// </summary>
        LastBossChase,
        /// <summary>
        /// 舊第一次追
        /// </summary>
        LastBossFirstChase,
        /// <summary>
        /// 舊後退，沒用到
        /// </summary>
        LastBossMoveBack,
        /// <summary>
        /// 舊Idle
        /// </summary>
        LastBossIdle,
        /// <summary>
        /// 新第一次Idle，一定會變第一次射，No.0
        /// </summary>
        LastBossFirstIdle,
        /// <summary>
        /// 新第一次射，No.4
        /// </summary>
        LastBossFirstShoot,
        /// <summary>
        /// 新Idle，No.1
        /// </summary>
        LastBossIdle1,
        /// <summary>
        /// 新追擊，普通追擊，No.2
        /// </summary>
        LastBossChase1,
        /// <summary>
        /// 新跳，No.7
        /// </summary>
        LastBossJump1,
        /// <summary>
        /// 新的攻擊，近攻，No.6
        /// </summary>
        LastBossAttackNew1,
        /// <summary>
        /// 新射大球，遠攻，No.5
        /// </summary>
        LastBossShootBigBall,
        LastBossDeath,
    }
    public abstract class State : MonoBehaviour
    {
        //印出這個狀態的名字
        private string GetThisClassName()
        {
            return this.GetType().Name;
        }
        //爸爸直接套用了印出狀態的名字，所以override打下去，base 使用父親的方法即可
        public virtual void EnterState(AI_data data)
        {
            Debug.Log(data.name.ToString() + "\tEnter\t" + GetThisClassName());
        }
        public virtual void OnState(AI_data data)
        {
            Debug.Log(data.name.ToString() + "\tIn\t" + GetThisClassName());
        }
        public virtual void ExitState(AI_data data)
        {
            Debug.Log(data.name.ToString() + "\tExit\t" + GetThisClassName());
        }
    }
    

    public class State_Idle : State
    {
        public override void EnterState(AI_data data)
        {

            Debug.Log("Enter Idle State: " + data.name.ToString());
        }
        public override void OnState(AI_data data)
        {
            AI_Function.Idle(data);
            Debug.Log("In Idle State : " + data.name.ToString());
        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit Idle State: " + data.name.ToString());
        }
    }

    public class State_Chase : State
    {
        public override void EnterState(AI_data data)
        {

            Debug.Log("Enter Chase State: " + data.name.ToString());
        }
        public override void OnState(AI_data data)
        {
            AI_Function.ToChase(data); 
            Debug.Log("In Chase State : " + data.name.ToString());
        }
        public override void ExitState(AI_data data)
        {
            //if (!AI_Function.CheckTargetEnemyInSight(data))
            //{
                AI_Function.Idle(data); // 兩支近戰會常駐IDLE 需解決
            //}
            //else if (AI_Function.InBackRange(data) == true)
            //{
            //    AI_Function.ToBack(data);
            //}
            //else
            //{
            //    AI_Function.ToChase(data);
            //}
            
            Debug.Log("Exit Chase State: " + data.name.ToString());
        }
    }

    public class State_Attack : State
    {
        int attackPattern;
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter Attack State: " + data.name.ToString());
            attackPattern = Random.Range(1, 3);
            switch (attackPattern)
            {
                case 1:
                    data.anim.SetTrigger("Attack1");
                    break;
                case 2:
                    data.anim.SetTrigger("Attack2");
                    break;

            }

        }
        public override void OnState(AI_data data)
        {
            AI_Function.MelleAttack(data);
            Debug.Log("In Attack State : " + data.name.ToString());
        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit Attack State: " + data.name.ToString());
        }
    }

    public class State_TakeDamage : State
    {
        float time = 0;
        float nextTime = 0.01f;
        public override void EnterState(AI_data data)
        {
            
            Debug.Log("Enter TakeDamage State: " + data.name.ToString());
        }
        public override void OnState(AI_data data)
        {
            time += Time.deltaTime;
            if (time > nextTime)
            {
                AI_Function.TakeDamage(data);
                time = 0;
            }

            Debug.Log("In TakeDamage State : " + data.name.ToString());
        }
        public override void ExitState(AI_data data)
        {
            AI_Function.Idle(data);
            Debug.Log("Exit TakeDamage State: " + data.name.ToString());
        }
        //private void OnTriggerEnter(Collider other)
        //{
            
        //}
    }

    public class State_Dead : State
    {
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter Dead State: " + data.name.ToString());

        }
        public override void OnState(AI_data data)
        {
            Debug.Log("In Dead State : " + data.name.ToString());
        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit Dead State: " + data.name.ToString());
        }
    }
    public class State_Back : State
    {
        public override void EnterState(AI_data data)
        {
            
        }
        public override void OnState(AI_data data)
        {
           if(data.currentHp > 0)
            {

            AI_Function.ToBack(data);
            }
            
        }
        public override void ExitState(AI_data data)
        {
            AI_Function.UnBack(data);

        }
    }
    public class State_UnBack : State
    {
        public override void EnterState(AI_data data)
        {
            
        }
        public override void OnState(AI_data data)
        {
            AI_Function.UnBack(data);

        }
        public override void ExitState(AI_data data)
        {
            //AI_Function.Idle(data); 
           // AI_Function.ToChase(data);

        }
    }

    public class State_Shoot : State
    {
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter Shoot State: " + data.name.ToString());
        }
        public override void OnState(AI_data data)
        {
            if (!AI_Function.Dead(data))
            {

            AI_Function.Shoot(data);
            }
            Debug.Log("In Shoot State : " + data.name.ToString());
        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit Shoot State: " + data.name.ToString());
        }
    }


    //////////////////以下大魔王專用的狀態//////////////////////

    /// <summary>
    /// LBState No.0
    /// </summary>
    public class State_LastBossFirstIdle : State
    {
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);
            data.f_LBTimer = 0f;//計時器歸0
            data.anim.SetBool("GoTo_Idle", true);//站的動畫
            data.anim.SetBool("MoveForward", false);//走路動畫
            data.f_LBMoveSpeed = 0f;//位移速度

            data.b_LBMoving = false;//不給走
            data.b_LBRotating = true;//給轉

        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);//計時開始
            if (data.f_LBTimer > 1.0f)//時間超過1.5強制跳出第一次Idle
            {
                data.i_LBStatePattern = 4;
            }

        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.f_LBTimer = 0;//計時器歸0
            data.anim.SetBool("GoTo_Idle", true);//站的動畫
            data.anim.SetBool("MoveForward", false);//走路動畫
            data.f_LBMoveSpeed = 0f;//位移速度

            data.b_LBMoving = false;//不給走
            data.b_LBRotating = true;//給轉
        }
    }

    /// <summary>
    /// LBState No.1
    /// </summary>
    public class State_LastBossIdle1 : State
    {
        public int i_LBHowManyTimesIdle;
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);
            i_LBHowManyTimesIdle++;
            data.f_LBTimer = 0;//計時器歸0
            data.anim.SetBool("GoTo_Idle", true);//站的動畫
            data.anim.SetBool("MoveForward", false);//走路動畫
            data.f_LBMoveSpeed = 0f;//位移速度

            data.b_LBMoving = false;//不給走
            data.b_LBRotating = true;//給轉
        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);
            AI_Function.Distance(data);
            data.f_LBdirToPlayer = AI_Function.Distance(data);


            //轉向相關
            if (data.f_LBdirToPlayer < 1.5f)
            {
                data.f_LBRotationSpeed = 0.009f;
            }
            else if (data.f_LBdirToPlayer >= 1.5f && data.f_LBdirToPlayer < 8f)
            {
                data.f_LBRotationSpeed = 0.01f;
            }
            else if (data.f_LBdirToPlayer >= 8f)
            {
                data.f_LBRotationSpeed = 0.05f;
            }
            AI_Function.LastBossRotate(data);


            //狀態遷移
            if (data.i_LBHowManyIdleTimesToShootBigBall == 0)
            {
                Debug.LogError("你忘了設定大魔王進入Idle幾次之後一定射一顆大球的次數了！！！這樣一直除以0會錯喔！" + "\n然後你也不可以設1，會一直射大球");
            }
            if (i_LBHowManyTimesIdle % data.i_LBHowManyIdleTimesToShootBigBall == 0)//如果進入Idle狀態的次數是倍數的話                
            {
                data.i_LBStatePattern = 5;//強制射一顆大球
            }
            else if (data.f_LBdirToPlayer <= data.f_LBMelleAttackRange2)//idle狀態的距離在近攻距離內的話，就直接給他進入攻擊狀態
            {
                data.i_LBStatePattern = 6;
            }
            else if (data.f_LBTimer > 1.0f)
            {
                data.i_LBStatePattern = 2;
            }

        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.f_LBTimer = 0;//計時器歸0
        }
    }

    /// <summary>
    /// LBState No.4
    /// 一次射三顆大球，爆炸之後的效能會爆，所以只限一次
    /// </summary>
    public class State_LastBossFirstShoot : State
    {
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);
            data.f_LBTimer = 0;

            data.anim.SetBool("MoveForward", false);
            data.anim.SetTrigger("Shoot_BigBall2");//射的動畫

            data.f_LBMoveSpeed = 0f;//位移速度

            data.b_LBMoving = false;//不給走
            data.b_LBRotating = true;//給轉


        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);

            if (data.f_LBTimer > 5.0f)
            {
                data.i_LBStatePattern = 1;
            }
        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.f_LBTimer = 0;
        }
    }

    /// <summary>
    /// LBState No.2
    /// </summary>
    public class State_LastBossChase1 : State
    {
        float f_LBChaseSpeed = 0.08f;
        float f_LBRotateSpeed = 0.08f;
        int i_LBOver5SecondsGoToState;
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);

            data.anim.SetBool("MoveForward", true);

            data.b_LBMoving = true;//可以位移            
            data.f_LBMoveSpeed = f_LBChaseSpeed;//一進來給個速度

            data.b_LBRotating = true;//可以轉
            data.f_LBRotationSpeed = f_LBRotateSpeed;//一進來給旋轉速度

            data.f_LBTimer = 0;//時間歸0

            i_LBOver5SecondsGoToState = Random.Range(1, 5);//進入狀態的時候就決定超過5秒的下一個狀態要去哪

        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);//計時器
            AI_Function.LastBossAvoidObstacle(data);//迴避障礙物

            //下面旋轉相關
            if (data.f_LBdirToPlayer < 4 && data.f_LBdirToPlayer > 1.5f)//與玩家太靠近的時候，避免旋轉暴走
            {
                data.b_LBRotating = true;
                data.f_LBRotationSpeed = 0.03f;
            }
            else if (data.f_LBdirToPlayer >= 4)//離開一定距離後普通的速度轉
            {
                data.b_LBRotating = true;
            }
            else if (data.f_LBdirToPlayer < 1.5f)//太貼近的時候不要轉
            {
                data.b_LBRotating = true;
                data.f_LBRotationSpeed = 0.01f;
            }
            AI_Function.LastBossRotate(data);

            //下面移動相關
            AI_Function.Distance(data);//一直計算距離
            data.f_LBdirToPlayer = AI_Function.Distance(data);//把距離存起來
            if (data.f_LBdirToPlayer < 80f)//小於某距離時，叫他走到某距離
            {
                AI_Function.LastBossMoveForward(data, data.f_LBMelleAttackRange2);//一定要走到近戰攻擊距離，但是時間到了沒走到也沒關係，一樣強制他跳出追擊狀態
            }


            //這邊強制跳出狀態

            if (AI_Function.Distance(data) < data.f_LBMelleAttackRange2)//近攻距離內砍兩下
            {
                data.i_LBStatePattern = 6;//近攻狀態
            }
            else if (data.f_LBTimer > 4.0f)//超過4秒強制跳一下，或射一發
            {
                switch (i_LBOver5SecondsGoToState)
                {
                    case 1:
                        data.i_LBStatePattern = 7;//跳
                        break;
                    case 2:
                    case 3:
                    case 4:
                        data.i_LBStatePattern = 5;//射大球
                        break;
                }
            }
        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.anim.SetBool("MoveForward", false);//離開關動畫
            data.f_LBTimer = 0.0f;
        }
    }

    /// <summary>
    /// LBState No.7 
    /// </summary>
    public class State_LastBossJump1 : State
    {
        int i_jumpPattern;
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);
            data.f_LBTimer = 0;//計時器歸0

            data.b_LBMoving = false;//不位移
            data.f_LBMoveSpeed = 0;//速度歸0
            data.anim.SetBool("MoveForward", false);
            data.b_LBRotating = false;//不旋轉

            i_jumpPattern = Random.Range(1, 3);
            switch (i_jumpPattern)
            {
                case 1:
                    data.anim.SetTrigger("Jump");//跳的動畫
                    break;
                case 2:
                    data.anim.SetTrigger("Jump_SameSpeed");//泡泡是一圈的
                    break;
            }

        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);

            //跳完之後回到Idle，離開狀態的時間長度=動畫的長度
            if (data.f_LBTimer > 3.2f)
            {
                data.i_LBStatePattern = 1;
            }

        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.f_LBTimer = 0f;
        }
    }

    /// <summary>
    /// LBState No.6
    /// </summary>
    public class State_LastBossAttack1 : State
    {
        int i_AttackPattern;
        public override void EnterState(AI_data data)
        {
            i_AttackPattern = Random.Range(1, 5);
            base.EnterState(data);
            data.f_LBTimer = 0;

            switch (i_AttackPattern)
            {
                case 1:
                    data.anim.SetTrigger("Attack2");//近戰砍兩下動畫開啟
                    break;
                case 2:
                    data.anim.SetTrigger("Attack1");//很不像進戰的動畫開啟，往下射一堆泡泡，泡泡要不等速
                    break;
                case 3:
                    data.anim.SetTrigger("Jump");//跳
                    break;
                case 4:
                    data.anim.SetTrigger("Jump_SameSpeed");//跳
                    break;
            }

            data.b_LBMoving = false;//不准動
            data.f_LBMoveSpeed = 0.0f;//速度歸0
            data.anim.SetBool("MoveForward", false);
            data.b_LBRotating = false;//不准轉


        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);

            //強制離開狀態，根據Random的結果，決定要停留多久
            switch (i_AttackPattern)
            {
                case 1:
                    if (data.f_LBTimer > 2.8f)
                    {
                        data.i_LBStatePattern = 1;
                    }
                    break;
                case 2:
                    if (data.f_LBTimer > 2.6f)
                    {
                        data.i_LBStatePattern = 1;
                    }
                    break;
                case 3:
                case 4:
                    if (data.f_LBTimer > 3.2f)
                    {
                        data.i_LBStatePattern = 1;
                    }
                    break;
            }

        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.f_LBTimer = 0;
        }
    }

    /// <summary>
    /// LBState No.5
    /// </summary>
    public class State_LastBossShootBigBall : State
    {
        int i_shootPattern;
        int i_shootBigBallCount;
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);
            data.f_LBTimer = 0;//計時器歸0
            i_shootBigBallCount++;
            data.b_LBEmitterRotate = false;
            //data.go_LBEmitterHolder.transform.rotation = Quaternion.Euler(0, 0, 0);

            data.b_LBMoving = false;//不准動
            data.f_LBMoveSpeed = 0.0f;

            data.b_LBRotating = false;//不准轉

            data.anim.SetBool("MoveForward", false);

            i_shootPattern = Random.Range(1, 8);

            if (i_shootBigBallCount % 5 == 0)
            {
                data.anim.SetTrigger("Shoot_BigBall2");
            }
            else
            {
                switch (i_shootPattern)
                {
                    case 1:
                        data.anim.SetTrigger("Shoot_Circle");
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        data.anim.SetTrigger("Shoot_BigBall");
                        break;
                    case 6:
                    case 7:
                        data.anim.SetTrigger("Shoot_Ternado");
                        break;
                }
            }



        }
        public override void OnState(AI_data data)
        {
            base.OnState(data);
            AI_Function.LastBossTimer(data);

            //根據動畫長度，播完動畫強制離開這個狀態
            if (data.f_LBTimer > 4.50f)
            {
                data.i_LBStatePattern = 1;
            }
        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
            data.f_LBTimer = 0;
        }
    }

    /// <summary>
    /// LBState No.9
    /// </summary>
    public class State_LastBossDeath : State
    {
        public override void EnterState(AI_data data)
        {
            base.EnterState(data);
            data.b_LBMoving = false;
            data.b_LBRotating = false;
            data.f_LBMoveSpeed = 0f;
            data.anim.SetBool("MoveForward", false);
            data.anim.SetBool("Go_Idle", false);
            data.anim.SetBool("Go_Dead", true);
        }

        public override void OnState(AI_data data)
        {
            base.OnState(data);
            data.anim.SetBool("Go_Dead", true);
        }
        public override void ExitState(AI_data data)
        {
            base.ExitState(data);
        }
    }


    /////////////////////////////////////////////////////////////
    /// <summary>
    /// LBStateNo.2
    /// </summary>
    public class State_LastBossMelleAttack1 : State
    {
        int i_LBAttackPatten;
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBAttack State: " + data.name.ToString());
            data.b_LBRotating = true;//一進狀態就准許他可以轉向
            data.b_LBMoving = false;
            data.anim.SetBool("MoveForward", false);
            data.f_LBTimer = 0;//進來要重新計時
            //data.i_LBMelleAttackPattern = 1;
            //data.anim.SetTrigger("Attack1");


            i_LBAttackPatten = Random.Range(1, 5);// 1=attack, 2&3&4=jump
            switch (i_LBAttackPatten)
            {
                case 1:
                    data.anim.SetTrigger("Attack1");
                    data.i_LBMelleAttackPattern = 1;
                    data.b_LBRotating = false;
                    data.b_LBMoving = false;
                    break;
                case 2:
                case 3:
                case 4:
                    data.anim.SetTrigger("Jump");
                    data.i_LBMelleAttackPattern = 3;
                    data.b_LBRotating = true;
                    data.b_LBMoving = false;
                    break;
            }
        }

        public override void OnState(AI_data data)
        {
            Debug.Log("In LastBAttack State: " + data.name.ToString());
            AI_Function.LastBossTimer(data);
            data.anim.SetBool("MoveForward", false);
            data.f_LBdirToPlayer = AI_Function.Distance(data);

            //旋轉相關
            data.f_LBRotationSpeed = 0.009f;
            AI_Function.LastBossRotate(data);

            //   //第一版 攻擊
            //   /* 
            //   //旋轉相關
            //   if (data.f_LBdirToPlayer < 4 && data.f_LBdirToPlayer > 1.5f)//與玩家太靠近的時候，避免旋轉暴走
            //   {
            //       data.b_LBRotating = true;
            //       data.f_LBRotationSpeed = 0.008f;
            //   }
            //   else if (data.f_LBdirToPlayer >= 4)//離開一定距離後普通的速度轉
            //   {
            //       data.b_LBRotating = true;
            //   }
            //   else if (data.f_LBdirToPlayer < 1.5f)//太貼近的時候不要轉
            //   {
            //       data.b_LBRotating = false;
            //   }
            //   AI_Function.LastBossRotate(data);

            //   //移動相關
            //   switch (i_LBAttackPatten)
            //   {
            //       case 1:
            //           AI_Function.LastBossMoveForward(data, data.f_LBMelleAttackRange1);
            //           if (data.f_LBdirToPlayer < data.f_LBMelleAttackRange1+0.3f)
            //           {
            //               data.anim.SetTrigger("Attack1");
            //               data.i_LBMelleAttackHowManyTimes += 1;
            //           }
            //           break;
            //       case 2:
            //           AI_Function.LastBossMoveForward(data, data.f_LBMelleAttackRange2);
            //           if (data.f_LBdirToPlayer < data.f_LBMelleAttackRange2+0.3f)
            //           { 
            //               data.anim.SetTrigger("Attack2");
            //               data.i_LBMelleAttackHowManyTimes += 1;
            //           }
            //           break;
            //}
            //           */

            //第二版攻擊，這版攻擊狀態裡面不做任何位移(正確版)

            //旋轉相關
            //if (data.f_LBdirToPlayer < 4 && data.f_LBdirToPlayer > 1.5f)//與玩家太靠近的時候，避免旋轉暴走
            //{
            //    data.b_LBRotating = true;
            //    data.f_LBRotationSpeed = 0.05f;
            //}
            //else if (data.f_LBdirToPlayer >= 4)//離開一定距離後普通的速度轉
            //{
            //    data.b_LBRotating = true;
            //}
            //else if (data.f_LBdirToPlayer < 1.5f)//太貼近的時候不要轉
            //{
            //    data.b_LBRotating = false;
            //}
            //data.f_LBRotationSpeed = 0.01f;
            //AI_Function.LastBossRotate(data);

            //跳出狀態相關
            switch (i_LBAttackPatten)
            {
                case 1:
                    if (data.f_LBTimer > 3.8f)//精密計算過的時間！
                    {
                        data.i_LBStatePattern = 5;
                    }
                    break;
                case 2:
                case 3:
                case 4:
                    if (data.f_LBTimer > 4.2f)//精密計算過的時間！
                    {
                        data.f_LBTimerTemporarySave = 0f;
                        data.i_LBStatePattern = 5;
                    }
                    break;
            }

        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBAttack State: " + data.name.ToString());

            data.f_LBTimer = 0;//離開狀態重新計時，不然緩衝會沒意義
            switch (i_LBAttackPatten)
            {
                case 1:
                    data.f_LBTimerTemporarySave = 0f;//等待的緩衝時間
                    break;
                case 2:
                case 3:
                case 4:
                    data.f_LBTimerTemporarySave = 0f;//等待的緩衝時間                    
                    break;
            }
            //data.f_LBTimer = 0;//計時器歸0
            //data.i_LBStatePattern = 1;//從裡面改變外面的攻擊狀態
        }
    }

    /// <summary>
    /// LBStateNo.3
    /// </summary>
    public class State_LastBossMelleAttack2 : State
    {

        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBAttack2 State: " + data.name.ToString());
            data.anim.SetBool("MoveForward", false);
            data.b_LBRotating = false;//一進狀態就的轉向
            data.b_LBMoving = false;
            data.anim.SetBool("MoveForward", false);
            data.f_LBTimer = 0;//進狀態重新計時

            data.i_LBMelleAttackPattern = 2;
            data.anim.SetTrigger("Attack2");


        }
        public override void OnState(AI_data data)
        {
            Debug.Log("In LastBAttack2 State: " + data.name.ToString());
            AI_Function.LastBossTimer(data);


            data.f_LBdirToPlayer = AI_Function.Distance(data);

            //data.b_LBMoving = false;
            //data.b_LBRotating = true;

            //旋轉相關
            //if (data.f_LBdirToPlayer < 4 && data.f_LBdirToPlayer > 1.5f)//與玩家太靠近的時候，避免旋轉暴走
            //{
            //    data.b_LBRotating = true;
            //    data.f_LBRotationSpeed = 0.05f;
            //}
            //else if (data.f_LBdirToPlayer >= 4)//離開一定距離後普通的速度轉
            //{
            //    data.b_LBRotating = true;
            //}
            //else if (data.f_LBdirToPlayer < 1.5f)//太貼近的時候不要轉
            //{
            //    data.b_LBRotating = false;
            //}
            //data.f_LBRotationSpeed = 0.01f;
            //AI_Function.LastBossRotate(data);

            if (data.f_LBTimer > 4.5f)//精密計算過的時間
            {
                data.i_LBStatePattern = 5;
            }

        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBAttack2 State: " + data.name.ToString());
            data.f_LBTimerTemporarySave = 0f;//換狀態的緩衝等待的時間
            data.f_LBTimer = 0;//離開狀態重新計時，不然緩衝會沒意義
            //data.i_LBStatePattern = 1;//從裡面改變外面的攻擊狀態
        }
    }

    //暫時沒用到
    public class State_LastBossJumpAttack : State
    {
        float attackTimer;
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBJumpAttack State: " + data.name.ToString());
            data.b_LBRotating = true;//一進狀態就准許他可以轉向
            data.b_LBMoving = false;
            data.anim.SetBool("MoveForward", false);
            //data.f_LBTimer = 0;
            attackTimer = 0;
            data.anim.SetTrigger("Jump");
        }

        public override void OnState(AI_data data)
        {
            Debug.Log("In LastBJumpAttack State: " + data.name.ToString());
            AI_Function.LastBossTimer(data);
            data.anim.SetBool("MoveForward", false);
            attackTimer += Time.deltaTime;
            data.i_LBMelleAttackPattern = 3;
            data.f_LBdirToPlayer = AI_Function.Distance(data);

            data.b_LBMoving = false;
            data.b_LBRotating = true;







            data.i_LBStatePattern = 1;


        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBJumpAttack State: " + data.name.ToString());
            data.f_LBTimerTemporarySave = 8.8f;


            data.f_LBTimer = 0;//計時器歸0
            //data.bubbleShooter.MakeBubbleWithPools(data.tr_LBJumpShoot);


            data.i_LBStatePattern = 1;//從裡面改變外面的攻擊狀態
        }
    }


    /// <summary>
    /// LBStateNo.4
    /// </summary>
    public class State_LastBossShoot : State
    {
        int shootPattern;

        
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBShoot State: " + data.name.ToString());

            shootPattern = Random.Range(1, 5);//1-4
            data.b_LBRotating = true;//一進狀態就准許他可以轉向
            data.b_LBMoving = false;
            data.anim.SetBool("MoveForward", false);
            data.f_LBMoveSpeed = 0;
            switch (shootPattern)
            {
                case 1:
                    data.i_LBShootAttackPattern = 1;
                    data.b_LBEmitterRotate = true;
                    data.anim.SetTrigger("Shoot_Circle");
                    break;
                //case 2:
                //    data.i_LBShootAttackPattern = 2;
                //    data.b_LBEmitterRotate = false;
                //    data.anim.SetTrigger("Shoot_Heart");
                //    break;
                case 2:
                    data.i_LBShootAttackPattern = 2;
                    data.b_LBEmitterRotate = true;
                    data.anim.SetTrigger("Shoot_Kome");
                    break;
                case 3:
                    data.i_LBShootAttackPattern = 3;
                    data.b_LBEmitterRotate = true;
                    data.anim.SetTrigger("Shoot_Triangle");
                    break;
                case 4:
                    data.i_LBShootAttackPattern = 4;
                    data.b_LBEmitterRotate = false;//大球的旋轉看是要在求身上還是這邊控制，球身上應該比較正確
                    data.anim.SetTrigger("Shoot_BigBall");
                    break;
                    
            }

            data.f_LBTimer = 0;//進來重新計時

            //射擊動作做完之後，在做動作的地方發射一波
            //現在改成射擊動作做完之後，在做動作的地方射出一顆大球，再由那顆球射出很多泡泡
            //data.float_FirstShootDelayTime = 3.0f;//射擊動作的時間剛好差不多3秒回到 Idle，所以中間大概是1.5f
            //那顆大球的射泡泡
            //shootPattern = Random.Range(1, 2);//隨機數抓取射擊樣式(大球裡面出泡泡的不同射擊方法)
            //大球會隨時間經過(看到時候要設幾秒)消失
            //data.anim.SetTrigger("Shoot");//啟動射擊動作
            //data.rb_LBrb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;

        }
        public override void OnState(AI_data data)
        {
            Debug.Log("In LastBShoot State: " + data.name.ToString());
            AI_Function.LastBossTimer(data);
            //讓發射口一直旋轉(看不到的)
            data.bubbleShooter.BubbleMakerRotate(data.go_LBEmitterHolder, 0, 0, 0.5f);

            data.f_LBMoveSpeed = 0;
            data.b_LBMoving = false;

            if (AI_Function.LastBossTimer(data) >= 4.5f)//精密計算過的時間，跳出射擊狀態
            {
                data.i_LBStatePattern = 5;
            }


            //旋轉相關
            if (AI_Function.Distance(data) > data.f_LBShootAttackRange)
            {
                data.f_LBRotationSpeed = 0.03f;
            }
            else if (AI_Function.Distance(data) <= data.f_LBShootAttackRange && AI_Function.Distance(data) > 2.5f)
            {
                data.f_LBRotationSpeed = 0.01f;
            }
            else if (AI_Function.Distance(data) <= 2.5f)
            {
                data.f_LBRotationSpeed = 0.001f;
            }
            //data.f_LBRotationSpeed = 0.01f;
            AI_Function.LastBossRotate(data);





            //data.float_FirstShootDelayTime -= Time.deltaTime;

            //data.anim.SetTrigger("Shoot");
            //if (data.float_FirstShootDelayTime < 0)
            //{
            //    switch (shootPattern)
            //    {
            //        case 1:
            //            AI_Function.ShootPattern1(data);
            //            break;
            //    }
            //}



        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBShoot State: " + data.name.ToString());

            data.f_LBMoveSpeed = 0;
            data.b_LBMoving = false;

            data.f_LBTimer = 0;//離開狀態重新計時，不然緩衝會沒意義

            //switch (shootPattern)
            //{
            //    case 1:
            //        data.anim.SetTrigger("Shoot_Circle");
            //        break;
            //    case 2:
            //        data.anim.SetTrigger("Shoot_Kome");
            //        break;
            //    case 3:
            //        data.anim.SetTrigger("Shoot_Triangle");
            //        break;
            //    case 4:
            //        data.anim.SetTrigger("Shoot_BigBall");
            //        break;
            //    //case 5:

            //    //    break;
            //}

            //data.i_LBStatePattern = 1;
            switch (shootPattern)
            {
                case 1:
                    data.i_LBShootAttackPattern = 1;
                    data.b_LBEmitterRotate = true;
                    //data.anim.SetTrigger("Shoot_Circle");
                    break;
                //case 2:
                //    data.i_LBShootAttackPattern = 2;
                //    data.b_LBEmitterRotate = false;
                //    data.anim.SetTrigger("Shoot_Heart");
                //    break;
                case 2:
                    data.i_LBShootAttackPattern = 2;
                    data.b_LBEmitterRotate = true;
                    //data.anim.SetTrigger("Shoot_Kome");
                    break;
                case 3:
                    data.i_LBShootAttackPattern = 3;
                    data.b_LBEmitterRotate = true;
                    //data.anim.SetTrigger("Shoot_Triangle");
                    break;
                case 4:
                    data.i_LBShootAttackPattern = 4;
                    data.b_LBEmitterRotate = false;//大球的旋轉看是要在求身上還是這邊控制，球身上應該比較正確
                    //data.anim.SetTrigger("Shoot_BigBall");
                    break;

            }
           //data.rb_LBrb.constraints = RigidbodyConstraints.None;   //BOSS 會突然不穩
            //data.rb_LBrb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            
            data.f_LBTimerTemporarySave = 3.5f;//等待的緩衝時間
        }
    }

    /// <summary>
    /// LBStateNo.5
    /// </summary>
    public class State_LastBossIdle : State
    {
        int i_LBidlecount;
        public override void EnterState(AI_data data)
        {
            data.anim.SetBool("GoTo_Idle", true);
            data.anim.SetBool("MoveForward", false);
            data.f_LBTimer = 0f;
            data.f_LBMoveSpeed = 0f;
            data.b_LBMoving = false;
            data.b_LBRotating = true;
            i_LBidlecount++;
        }
        public override void OnState(AI_data data)
        {
            AI_Function.LastBossTimer(data);

            
            
                if (AI_Function.LastBossTimer(data) > 1.0f)
                {
                    if (AI_Function.Distance(data) < data.f_LBMelleAttackRange2)
                    {
                        data.i_LBStatePattern = 2;
                    }
                    else /*if (AI_Function.Distance(data) < 80.0f && AI_Function.Distance(data) >= data.f_LBMelleAttackRange2)*/
                    {
                        data.i_LBStatePattern = 1;
                    }

                }
            
            
        }

        public override void ExitState(AI_data data)
        {
            data.f_LBTimer = 0f;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class State_LastBossMoveBackward : State
    {

        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBMoveBack State: " + data.name.ToString());
            data.b_LBRotating = true;//一進狀態就准許他可以轉向
            data.b_LBMoving = true;
            data.anim.SetBool("MoveBackward", false);
            data.f_LBTimer = 0;


        }
        public override void OnState(AI_data data)
        {
            Debug.Log("In LastBMoveBack State: " + data.name.ToString());

            data.i_LBStatePattern = 4;
        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBMoveBack State: " + data.name.ToString());

            data.i_LBStatePattern = 4;
        }

    }

    /// <summary>
    /// LBStateNo.1
    /// </summary>
    public class State_LastBossChase : State
    {
        float f_LBChaseSpeed;
        int i_LBChaseTransState;
        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBChase State: " + data.name.ToString());
            data.b_LBRotating = true;//一進狀態就不准許他可以轉向
            data.b_LBMoving = true;
            data.anim.SetBool("MoveForward", false);
            f_LBChaseSpeed = 0.07f;
            data.f_LBMoveSpeed = f_LBChaseSpeed; 
            data.f_LBTimer = 0;//進狀態計時器歸0
            i_LBChaseTransState = Random.Range(2, 5);//從裡面改變外面的攻擊狀態
            //根據攻擊狀態不同有不同的攻擊距離
            switch (i_LBChaseTransState)
            {
                //短攻擊 & 跳
                case 2:
                    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange1;
                    data.b_LBRotating = true;
                    data.b_LBMoving = true;
                    break;
                //長攻擊
                case 3:
                    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange2;
                    data.b_LBRotating = true;
                    data.b_LBMoving = true;
                    break;
                //射
                case 4:
                    data.f_LBDeciedAttackRange = data.f_LBShootAttackRange;
                    data.b_LBMoving = true;
                    data.b_LBRotating = true;
                    break;
                    ////跳
                    //case 3:
                    //    data.f_LBMelleAttackDeciedRange = data.f_LBMelleAttackRange1;
                    //    break;
            }

        }
        public override void OnState(AI_data data)
        {

            Debug.Log("In LastBChase State: " + data.name.ToString());
            AI_Function.LastBossTimer(data);

            AI_Function.LastBossAvoidObstacle(data);
            AI_Function.Distance(data);
            data.f_LBdirToPlayer = AI_Function.Distance(data);

            if (data.f_LBdirToPlayer < data.f_LBMelleAttackRange2)
            {
                data.anim.SetBool("MoveForward", false);
            }

            data.anim.SetBool("MoveForward", true);//打開動畫





            //旋轉相關
            if (data.f_LBdirToPlayer < 4 && data.f_LBdirToPlayer > 1.5f)//與玩家太靠近的時候，避免旋轉暴走
            {
                data.b_LBRotating = true;
                data.f_LBRotationSpeed = 0.05f;
            }
            else if (data.f_LBdirToPlayer >= 4)//離開一定距離後普通的速度轉
            {
                data.b_LBRotating = true;
            }
            else if (data.f_LBdirToPlayer < 1.5f)//太貼近的時候不要轉
            {
                data.b_LBRotating = false;
            }
            AI_Function.LastBossRotate(data);

            //移動相關


            //根據攻擊狀態不同有不同的攻擊距離
            switch (i_LBChaseTransState)
            {
                //短攻擊 & 跳
                case 2:
                    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange1;
                    data.b_LBRotating = true;
                    data.b_LBMoving = true;
                    break;
                //長攻擊
                case 3:
                    data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange2;
                    data.b_LBRotating = true;
                    data.b_LBMoving = true;
                    break;
                //射
                case 4:
                    data.f_LBDeciedAttackRange = data.f_LBShootAttackRange;
                    data.b_LBMoving = true;
                    data.b_LBRotating = true;
                    break;
                    ////跳
                    //case 3:
                    //    data.f_LBMelleAttackDeciedRange = data.f_LBMelleAttackRange1;
                    //    break;
            }


            if (data.f_LBdirToPlayer < 80f)//移動到平台之後，在移動到某距離之後才開始追人
            {
                AI_Function.LastBossMoveForward(data, data.f_LBDeciedAttackRange);
            }

            //變更狀態相關

            switch (i_LBChaseTransState)
            {
                case 2:
                    if (AI_Function.LastBossTimer(data) > 4.0f)
                    {
                        data.i_LBStatePattern = 2;
                    }
                    break;
                case 3:
                    if (AI_Function.LastBossTimer(data) > 4.5f)
                    {
                        data.i_LBStatePattern = 3;
                    }
                    break;
                case 4:
                    if (AI_Function.LastBossTimer(data) > 4.5f)
                    {
                        data.i_LBStatePattern = 4;
                    }
                    break;
            }

            //不管距離有沒有追到，或是秀逗了，都強制他離開，重新再近來Random一個
            if (AI_Function.LastBossTimer(data) > 4.5f)
            {
                //data.b_LBMoving = false;
                //data.b_LBRotating = false;
                //data.anim.SetTrigger("Jump");

                //data.f_LBTimer = 0;
                //if (AI_Function.LastBossTimer(data)>4.0f)
                //{
                //    data.b_LBMoving = false;
                //    data.b_LBRotating = false;
                //data.i_LBStatePattern = 2;
                //}

                i_LBChaseTransState = Random.Range(1, 3);
                switch (i_LBChaseTransState)
                {
                    case 1:
                        data.b_LBMoving = true;
                        data.b_LBRotating = true;
                        data.i_LBStatePattern = 2;
                        break;
                    case 2:
                        data.b_LBMoving = true;
                        data.b_LBRotating = true;
                        data.i_LBStatePattern = 4;
                        break;
                }
            }

        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBChase State: " + data.name.ToString());
            data.anim.SetBool("MoveForward", false);
            data.b_LBRotating = false;
            data.b_LBMoving = false;
            data.f_LBTimer = 0;//計時器歸0
            /*data.i_LBStatePattern = Random.Range(2, 5);*///從裡面改變外面的攻擊狀態
            data.f_LBTimerTemporarySave = 0.0f;
        }
    }

    /// <summary> 
    /// LBStateNo.0
    /// 不知為何，第一次攻擊如果亂數選到跳，會在跳的動作結束前噴泡泡
    /// 所以弄一個State混水摸魚，這個state只會執行一次，非亂數的強制變更狀態到不會是跳的
    /// 目前預設attack2
    /// </summary>
    public class State_LastBossFirstChase : State
    {

        public override void EnterState(AI_data data)
        {
            Debug.Log("Enter LastBChase State: " + data.name.ToString());
            data.b_LBRotating = true;//一進狀態就准許他可以轉向
            data.b_LBMoving = true;
            data.anim.SetBool("MoveForward", true);
            data.f_LBTimer = 0;
            data.i_LBStatePattern = 3;//從裡面改變外面的攻擊狀態
            //根據攻擊狀態不同有不同的攻擊距離
            //switch (data.i_LBStatePattern)
            //{
            //    //短攻擊 & 跳
            //    case 1:
            //        data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange1;
            //        break;
            //    //長攻擊
            //    case 2:
            //        data.f_LBDeciedAttackRange = data.f_LBMelleAttackRange2;
            //        break;
            //        ////跳
            //        //case 3:
            //        //    data.f_LBMelleAttackDeciedRange = data.f_LBMelleAttackRange1;
            //        //    break;
            //}

        }
        public override void OnState(AI_data data)
        {

            Debug.Log("In LastBChase State: " + data.name.ToString());
            AI_Function.LastBossTimer(data);

            AI_Function.LastBossAvoidObstacle(data);
            AI_Function.Distance(data);
            data.f_LBdirToPlayer = AI_Function.Distance(data);

            data.anim.SetBool("MoveForward", true);//打開動畫

            //旋轉相關
            if (data.f_LBdirToPlayer < 4 && data.f_LBdirToPlayer > 1.5f)//與玩家太靠近的時候，避免旋轉暴走
            {
                data.b_LBRotating = true;
                data.f_LBRotationSpeed = 0.05f;
            }
            else if (data.f_LBdirToPlayer >= 4)//離開一定距離後普通的速度轉
            {
                data.b_LBRotating = true;
            }
            else if (data.f_LBdirToPlayer < 1.5f)//太貼近的時候不要轉
            {
                data.b_LBRotating = false;
            }
            AI_Function.LastBossRotate(data);

            //移動相關
            if (data.f_LBdirToPlayer < 80f)//移動到平台之後，在移動到某距離之後才開始追人
            {
                AI_Function.LastBossMoveForward(data, data.f_LBDeciedAttackRange);
            }


        }
        public override void ExitState(AI_data data)
        {
            Debug.Log("Exit LastBChase State: " + data.name.ToString());
            data.anim.SetBool("MoveForward", false);
            data.b_LBRotating = false;
            data.b_LBMoving = false;
            data.f_LBTimer = 0;//計時器歸0
            data.i_LBStatePattern = 3;//從裡面改變外面的攻擊狀態
        }
    }





    #endregion

    /// <summary>
    /// 各種GoTo什麼狀態的標籤
    /// </summary>

    #region 串連用的條件
    public class Transition
    {
        public StateType goToThisState { get; set; }
        public TransitionType transitionType { get; set; }
    }

    public enum TransitionType
    {
        GoTo_Idle,
        GoTo_Chase,
        GoTo_Attack,
        GoTo_TakeDamage,
        GoTo_Dead,
        GoTo_Shoot,
        GoTo_Back,
        GOTo_UnBack,
        /// <summary>
        /// 舊近戰1，要改射
        /// </summary>
        GoTo_LastBossAttack1,
        /// <summary>
        /// 舊近戰2，雙擊
        /// </summary>
        GoTo_LastBossAttack2,
        /// <summary>
        /// 舊追
        /// </summary>
        GoTo_LastBossChase,
        /// <summary>
        /// 舊第一次追
        /// </summary>
        GoTo_LastBossFirstChase,
        /// <summary>
        /// 舊射
        /// </summary>
        GoTo_LastBossShoot,
        /// <summary>
        /// 舊跳，沒用到
        /// </summary>
        GoTo_LastBossJump,
        /// <summary>
        /// 舊往後退，沒用到
        /// </summary>
        GoTo_LastBossMoveBack,
        /// <summary>
        /// 舊Idle
        /// </summary>
        GoTo_LastBossIdle,
        /// <summary>
        /// 新第一次Idle，No.0
        /// </summary>
        GoTo_LastBossFirstIdle,
        /// <summary>
        /// 新第一次射，三顆球，No.4
        /// </summary>
        GoTo_LastBossFirstShoot,
        /// <summary>
        /// 新Idle，No.1
        /// </summary>
        GoTo_LastBossIdle1,
        /// <summary>
        /// 新的跳的狀態，No.7
        /// </summary>
        GoTo_LastBossJump1,
        /// <summary>
        /// 新的追擊狀態，普通的追擊狀態，No.2
        /// </summary>
        GoTo_LastBossChase1,
        /// <summary>
        /// 新的攻擊，近攻，No.6
        /// </summary>
        GoTo_LastBossAttackNew1,
        /// <summary>
        /// 新射大球，遠攻，No.5
        /// </summary>
        GoTo_LastBossShootBigBall,
        GoTo_LastBossDeath
    }
    #endregion


}
