﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Bubble;
namespace SA
{
    public class AI_data : MonoBehaviour
    {
        public GameObject enemy;//這個怪獸
        public BubbleShoot_3 bubbleShooter;//這個射擊機
        public GameObject controller;
        public EnemyTarget enemyTarget;
        public FightSystem fightSystem;

        [Header("=====追蹤移動迴避=====")]
        public float rayDistance = 2.0f;
        // Start is called before the first frame update
        public GameObject target;
        public float Chease_speed = 0.2f;//追擊的速度
        //public float AttackRange = 1.0f;//攻擊範圍
        public float ChaseRange = 1.0f;//追擊範圍
        public float nextChaseSpeed;
        public float backSpeed;
        public float backRang;
        public float backTime;
        public float backRate;
        public float SightRange = 1.0f;//視角範圍
        public float Rotationspeed = 0.2f;//轉動速度
        [HideInInspector] public Rigidbody rb;
        public Vector3 movementVec;//往前的速度
        public Vector3 dirToPlayer;//npc跟角色的向量
        public bool isChase;
        public bool inBackRange;
        public bool isBack;
        public float float_nextChase;
        public float float_chaseRate;
        [Header("=====血量=====")]
        public float maxHp;
        public float currentHp;
        public bool isDead;
        [Header("=====主角的傷害=====")]
        public float C_Damage;
        public int onTriCount;
        [Header("=====距離判定=====")]


        public float float_Distance; //敵我雙方距離
        public float float_checkInSight; //檢查敵人是否進入警戒範圍
        public float float_SlowDownDistance;
        public float float_StopDistance;

        [Header("=====近戰攻擊力=====")]
        public float melleAttackD_Damage;
        public float melleAttack_Range;

        [Header("=====遠程攻擊力=====")]
        public float shoot_Damage;


        [Header("=====攻擊間隔=====")]
        public float attackRate;
        public float nextAttack;




        #region Move所需要的變數
        [Header("=====各種移動設定=====")]
        public bool bool_EnemyIsMoving;//移動狀態與否   

        public Vector3 vec_EnemyCurrentVector;//怪獸現在的向量
        public float float_EnemyCurrentMoveForce;//怪獸移動的力道
        public float float_EnemyCurrentMoveSpeed;//怪獸現在的移動速度
        public float float_EnemyMaxSpeed;//怪獸的最大移動速度

        [Header("=====各種轉向設定=====")]
        public float float_EnemyMaxRotateSpeed;//怪獸最大的轉向速度
        public float float_EnemyTempRotateForce;//怪獸暫時的轉向力道


        #endregion

        #region 攻擊&射擊所需要的變數
        [Header("=====是否進入攻擊狀態=====")]
        public bool bool_Attack;
        [Header("=====各種射擊設定=====")]
        public bool bool_EnemyHasRemoteAttackProperty;//是否具有遠攻屬性
        public bool bool_RotateShootProperty;//是否具有旋轉射擊的屬性

        //
        public float float_BubbleShootSpeed;//泡泡射擊速度，數字越大越慢(時間是倒數的)
        public float float_FirstShootDelayTime;//第一次射泡泡的延遲時間
        public float float_WaveShootTotalTime;//一波泡泡射擊總時間
        public float float_WaveShootOverDelayTime;//泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波
        public Transform[] transArray_Emiiter;//泡泡發射口
        public int int_BubbleDamage;//泡泡的傷害

        //
        public float float_ShootingDistance;//射擊距離
        public float float_RotationSpeed;//砲台瞄準速度
        [Header("=====護盾=====")]
        public Shield defenseShield;
        public bool HaveShield;
        public GameObject Shield;
        public bool shieldOpen;
        public float shieldRadius;
        public float shieldPower;

        [Header("=====各種近戰設定=====")]   // YEN
        public bool bool_EnemyHasMelleAttackProperty; //是否為近戰
        public bool bool_InAttackRange; //是否進入攻擊範圍
        public float float_LookRotationSpeed; //進入某警戒範圍後 會著面朝主角
        public float float_AttackRange; //攻擊範圍
        public float float_FirstAttackCountDown;
        public float float_FirstAttackDelayTime; //遇敵後的第一次攻擊時間
        public float float_MelleAttackRate; //攻擊間隔
        public float float_NextMelleAttack; //下一次的攻擊時間

        public int int_MelleAttack_Damage;//近戰攻擊傷害
        public string[] MelleAttackAnim; //攻擊的動畫陣列

        public Animator anim;

        [Header("=====承受傷害=====")]
        public bool takeDamage;
        public bool machineDamage;
        [Header("=====ＴＩＭＥ=====")]
        public bool IsFirstTimeAttack;//是不是第一次攻擊
        public bool IsFirstTimeShoot;
        #endregion

        #region 大魔王所需要的變數

        [Header("====大魔王設定====")]
        public float f_LBMoveSpeed;
        public float f_LBRotationSpeed;
        [HideInInspector] public float f_LBdirToPlayer;//監控用
        public float f_LBTimer;
        [HideInInspector] public float f_LBTimerTemporarySave;//暫時存時間用的，要進入到下個狀態要等幾秒(動畫時間等待用)
        public int i_LBStatePattern;//大魔王的狀態哪一種，進入狀態的條件(為了random)
        [HideInInspector] Vector3 v_LBVectorToPlayer;//向量和上面的追逐共用
        public bool b_LBRotating;//是否在旋轉
        public bool b_LBMoving;//是否在移動
        [HideInInspector] public bool b_LBJumping;
        [HideInInspector] public bool b_LBAttacking;
        [HideInInspector] public Vector3 v_LBVectorChase;//往前追的向量
        public Rigidbody rb_LBrb;
        [HideInInspector] public float f_LBAttackPrepareRange;//大魔王的攻擊距離
        public float f_LBMelleAttackRange1;//跳攻擊和短攻擊的距離，攻擊狀態1
        public float f_LBMelleAttackRange2;//常攻擊距離，攻擊狀態2
        [HideInInspector] public float f_LBDeciedAttackRange;//暫時儲存距離用的變數
        public float f_LBShootAttackRange;//射擊距離
        [HideInInspector] public int i_LBMelleAttackPattern;//random出了哪個攻擊，監看用
        [HideInInspector] public int i_LBShootAttackPattern;//遠攻樣式，監看用
        //public string[] s_LBShootAttackPatternAnimation;
        public bool b_LBEmitterRotate;//射擊時的旋轉口是否旋轉
        [HideInInspector] public float f_LBLeaveStateDistance;//離開狀態時的距離
        public int i_LBHowManyIdleTimesToShootBigBall;//設定幾次的Idle之後一定要射大球

        [Header("大魔王發射泡泡的群組")]
        ///
        ///<summary>各emitter的爸爸=emitterholder</summary>
        public GameObject go_LBEmitterHolder;
        public GameObject go_LBAttackEmitterHolder;

        public GameObject[] go_LBJumpShoot;

        public GameObject[] go_LBShootCircle;

        public GameObject[] go_LBShootKome;

        public GameObject[] go_LBShootStar;

        public GameObject[] go_LBShootTriangle;

        public GameObject[] go_LBShootBigBall;
        public GameObject[] go_LBShootBigBall2;

        public GameObject[] go_LBShootAttack1;

        public GameObject[] go_LBShootBigBallTernado;

        //下面存成Transform

        ////發射泡泡的各個函式
        //public void LastBossJumpShoot()
        //{
        //    bubbleShooter.MakeBubbleWithPools(tr_LBJumpShoot);
        //}


        #endregion


        public void OnDrawGizmos()
        {
            Vector3 thisEnemyPosition = this.transform.position;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(thisEnemyPosition, float_AttackRange);//劃出進戰攻擊範圍

            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(thisEnemyPosition, float_checkInSight);//劃出進戰攻擊範圍

            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(thisEnemyPosition, float_SlowDownDistance);

            //Gizmos.color = Color.red;
            //Gizmos.DrawWireSphere(this.transform.position, AttackRange);//攻擊範圍
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * rayDistance);

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(thisEnemyPosition, backRang);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);//警戒範圍

            Gizmos.color = Color.magenta;
            //Gizmos.DrawWireSphere(this.transform.position, ChaseRange);//追擊範圍
            //Vector3 g1Rector = Quaternion.AngleAxis(-45f, Vector3.up) * transform.forward;

            //Vector3 g2Rector = Quaternion.AngleAxis(-22.5f, Vector3.up) * transform.forward;
            //Vector3 g3Rector = Quaternion.AngleAxis(22.5f, Vector3.up) * transform.forward;
            //Vector3 g4Rector = Quaternion.AngleAxis(45f, Vector3.up) * transform.forward;
            ////旁邊兩條

            //Gizmos.color = Color.magenta;
            //Gizmos.DrawLine(this.transform.position, this.transform.position + g1Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position, this.transform.position + g4Rector * SightRange);
            ////
            //Gizmos.DrawLine(this.transform.position + g1Rector * SightRange, this.transform.position + g2Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position + g2Rector * SightRange, this.transform.position + g3Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position + g3Rector * SightRange, this.transform.position + g4Rector * SightRange);
            LastBossGizmos();
        }
        #region 魔王的線
        private void LastBossGizmos()
        {


            //Gizmos.color = Color.cyan;
            //Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 5.0f);

            //Gizmos.color = Color.white;
            //Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);

            if (enemyTarget == null)
            {
                return;
            }

            if (enemyTarget.currentState == StateType.Idle)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossFirstIdle)
            {
                Gizmos.color = new Color(0, 0.8f, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossFirstShoot)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossChase1)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossJump1)
            {
                Gizmos.color = new Color(1.0f, 0.5f, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossAttackNew1)
            {
                Gizmos.color = new Color(1.0f, 0, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossShootBigBall)
            {
                Gizmos.color = new Color(1.0f, 0.6f, 0.6f);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossDeath)
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
        }
        #endregion








        private void Awake()
        {

            //AI_data一開始找東西的時候，先找自己，把該要的參考資料都找到
            enemy = this.gameObject;
            bubbleShooter = enemy.GetComponentInChildren<BubbleShoot_3>();
            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
            anim = GetComponent<Animator>();
            controller = GameObject.Find("Controller");
            currentHp = maxHp;
            rb = GetComponent<Rigidbody>();
            target = GameObject.Find("Controller");
            defenseShield = enemy.GetComponentInChildren<Shield>();
            rb_LBrb = GetComponent<Rigidbody>();
            FindLastBossEmitters();//自動找發射點的函式，大魔王用
            

        }






        // Update is called once per frame
        void Update()
        {
            

        }


        public void OnTriggerEnter(Collider other) //只能在第一個CLASS觸發
        {

            if (other.gameObject.tag == "Weapon_point") /// 必須將主角武器設為"Weapon_point" !! 避免其他碰撞體傷害敵人
            {
                onTriCount++;
                fightSystem.attack = true;
                print("命中敵人");
                takeDamage = true;
                currentHp -= fightSystem.UpGradeDamage();
                anim.SetTrigger("take damageTri");
                
                //print("敵人當前血量 : " + currentHp);
            }

            //else
            //{
            //    //fightSystem.attack = false;
            //    anim.SetTrigger("UndamageTri");
            //    takeDamage = false;
            //}

            //bug : 射擊後  和武器一同造成傷害


            if (other.gameObject.tag == "Bullet") /// BUG: 會一直造成損傷 就算沒有射擊  
            {
                machineDamage = true;
                currentHp -= fightSystem.machineDamage;
                anim.SetTrigger("take damageTri");
                print("machineDamage");
                //takeDamege = false;
            }
            else
            {
                anim.SetTrigger("UndamageTri");
                machineDamage = false;
            }

            
        }
        public void OnTriggerExit(Collider other) //  未 執行攻擊時 將weaponTrigger的attackTri 給關閉 //似乎邏輯不通?
        {
            if (other.gameObject.tag == "Weapon_point")
            {
                fightSystem.attack = false;
                takeDamage = false;
                machineDamage = false;
            }
                
            
        }

        public float GetHealthRate()
        {
            return currentHp / maxHp;
        }
        //int IComparable<AI_data>.CompareTo(AI_data other)   // 使用IComparable 時必須加上去
        //{
        //    throw new NotImplementedException();
        //}

        public void FindLastBossEmitters()
        {
            #region 大魔王的發射點群組
            go_LBJumpShoot = GameObject.FindGameObjectsWithTag("JumpEmitter");
            go_LBShootBigBall = GameObject.FindGameObjectsWithTag("BigBallEmitter");
            go_LBShootCircle = GameObject.FindGameObjectsWithTag("CircleEmitter");
            go_LBShootKome = GameObject.FindGameObjectsWithTag("KomeEmitter");
            go_LBShootStar = GameObject.FindGameObjectsWithTag("StarEmitter");
            go_LBShootTriangle = GameObject.FindGameObjectsWithTag("TriangleEmitter");
            go_LBShootBigBallTernado = GameObject.FindGameObjectsWithTag("TernadoEmitter");
            go_LBShootAttack1 = GameObject.FindGameObjectsWithTag("ShootAttack1Emitter");
            go_LBEmitterHolder = GameObject.FindGameObjectWithTag("LB_EmitterHolder");

            go_LBAttackEmitterHolder = GameObject.FindGameObjectWithTag("LB_AttackEmitterHolder");
            go_LBShootBigBall2 = GameObject.FindGameObjectsWithTag("BigBallEmitter2");

            #endregion
        }

    }

    

}


    




