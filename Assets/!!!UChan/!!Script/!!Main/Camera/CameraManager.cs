﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class CameraManager : MonoBehaviour
    {
        public static CameraManager singleton;

        public bool lockon;
        public float followSpeed = 9;
        public float mouseSpeed = 2;
        public float controllerSpeed = 7;
        public float lookAngle;
        public float titleAngle;

        public Transform target;
        public Transform lockonTarget;

        [HideInInspector]
        public Transform pivot;
        [HideInInspector]
        public Transform camTrans;

        private float m_TurnSpeed = 0.2f; // How fast the rig will turn to keep up with target's rotation

        private Vector3 m_RollUp = Vector3.up;

        private Quaternion m_OriginalRotation;

        float turnSmoothing = 0.1f;
        public float minAngle = -35f;
        public float maxAngle = -35f;
        float smoothX;
        float smoothY;
        float smoothXvelocity;
        float smoothYvelocity;

        Quaternion rollRotation;


        public void Init(Transform t)
        {
            target = t;
            camTrans = Camera.main.transform;
            m_OriginalRotation = transform.localRotation; //
            pivot = camTrans;
        }

        public void Tick(float d)
        {
            float h = Input.GetAxis("Mouse X");
            float v = Input.GetAxis("Mouse Y");

            float c_h = Input.GetAxis("RightAxis X");
            float c_v = Input.GetAxis("RightAxis Y");

            float targetSpeed = mouseSpeed;
            if (c_h != 0 || c_v != 0)
            {
                h = c_h;
                v = -c_v;
                targetSpeed = controllerSpeed;
            }
            FollowTarget(d);
            //HandleRotations(d, v, h, targetSpeed);
            //FollowTarget(d);
            //攝影機追隨時的位置設定

            //if (((Input.GetKey(KeyCode.A))) || ((Input.GetKey(KeyCode.D))))//鍵盤用
            if ((Input.GetAxis("Horizontal") > 0.9f) || (Input.GetAxis("Horizontal") < -0.9f))//搖桿用
            {
                HandleRotations(d, v, h, targetSpeed);//攝影機追隨時可以環繞設定
                lookAngle = 0;

                rollRotation = Quaternion.LookRotation(target.forward, m_RollUp);
                transform.rotation = Quaternion.Lerp(transform.rotation, rollRotation, m_TurnSpeed * 3 * d);
                //print("transform.rotation ad" + transform.rotation);
            }
            else if (h > 0)//滑鼠移動往右轉視角
            {
                HandleRotations(d, v, h, targetSpeed);//攝影機追隨時可以環繞設定

                // transform.rotation = transform.rotation;
                //rr2 = Quaternion.Euler(0, lookAngle, 0);
                
                lookAngle = h;
                
            }
            else if (h < 0)//滑鼠移動往右轉視角
            {
                HandleRotations(d, v, h, targetSpeed);//攝影機追隨時可以環繞設定

                // transform.rotation = transform.rotation;
                //rr2 = Quaternion.Euler(0, lookAngle, 0);
                
                lookAngle = h;
                
            }
            else if (h == 0)//滑鼠不動不轉視角
            {

                HandleRotations(d, v, h, targetSpeed);//攝影機追隨時可以環繞設定

                lookAngle = 0;
                //print("停止轉動" + h + "lookAngle" + lookAngle);
            }

            transform.position = Vector3.Lerp(transform.position, target.position, d);
            transform.rotation = transform.rotation * Quaternion.Euler(0, lookAngle, 0);
            //print("transform.rotation total" + transform.rotation);
        }
        void FollowTarget(float d)
        {
            float speed = followSpeed * d;
            Vector3 targetPosition = Vector3.Lerp(transform.position, target.position, speed);
            transform.position = targetPosition;

        }

        void HandleRotations(float d, float v, float h, float targetSpeed)
        {
            //camTrans = Camera.main.transform;
            if (turnSmoothing > 0)
            {
                smoothX = Mathf.SmoothDamp(smoothX, h, ref smoothXvelocity, turnSmoothing);
                smoothY = Mathf.SmoothDamp(smoothY, v, ref smoothYvelocity, turnSmoothing);
            }
            else
            {
                smoothX = h;
                smoothX = v;
            }


            titleAngle -= smoothY * targetSpeed;
            titleAngle = Mathf.Clamp(titleAngle, minAngle, maxAngle);
            pivot.localRotation = Quaternion.Euler(titleAngle, 0, 0);

            lookAngle += smoothX * targetSpeed;
            
                

            if (lockon && lockonTarget != null)
            {
                Vector3 targetDir = lockonTarget.position - this.transform.position;
                targetDir.Normalize();  //Vector3.Normalize(); 當前向量為1

                if (targetDir == Vector3.zero)
                    targetDir = transform.forward;
                Quaternion targetRot = Quaternion.LookRotation(targetDir);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, d * 9);//鎖定後的角度
               // print(lockonTarget);//
               // lockon = true; //目標鎖定後的bool 保持
                return;
            }
            
            

        }
        private void Awake()
        {
            singleton = this;
            
        }
    }
}
