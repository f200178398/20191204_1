﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

namespace Bubble
{
    public class BigBall_1 : MonoBehaviour
    {
        //public FightSystem fightSystem;
        //public GameObject go_FightSystem;
        //public AI_data data;
        //public GameObject go_data;

        public GameObject go_pool;
        public BubblePool pool;
        public GameObject go_sameSpeedPool;
        public SameSpeedBubblePool sameSpeedPool;
        // Start is called before the first frame update
        private void Awake()
        {
            //go_FightSystem = GameObject.Find("FightSystem");
            //fightSystem = go_FightSystem.GetComponent<FightSystem>();
            //go_data=this.gameObject;
            //data=go_data.GetComponent<AI_data>(); 

            go_pool = GameObject.FindGameObjectWithTag("BubblePool");
            pool = go_pool.GetComponent<BubblePool>();
            go_sameSpeedPool = GameObject.FindGameObjectWithTag("SameSpeedBubblePool");
            sameSpeedPool = go_sameSpeedPool.GetComponent<SameSpeedBubblePool>();

            controller = GameObject.FindGameObjectWithTag("Controller");
            dirToPlayer = controller.transform.position - this.transform.position;

            bubbleSpeed = Random.Range(bubbleSpeedRandomMin, bubbleSpeedRandomMax);//生成時隨機給他速度

        }

        void Start()
        {
            //bubbleShootPattern = Random.Range(1, 10);
            


        }

        private void FixedUpdate()
        {
            //BubbleLife();

            //大球自己
            //BigBallMove();
            BigBallSlowDownMove();
            BigBallLifeReturnToPool();//大球自己回到物件池
            BigBallShrink();
            BubbleShootWithPattern();
            //BigBallMovePlayer();
            
            //泡泡相關

        }

        #region Bubble Life
        void OnEnable()//物件被Active的時候紀錄物件當時的情況
        {
            _timer = Time.time;//開始計時
        }

        [Header("Bubble Life Settings")]
        public float _timer;
        public float bubbleLife = 10.0f;

        void BubbleLife()//原版的泡泡生命，生命沒了直接Destory自己
        {
            bubbleLife -= Time.deltaTime;//泡泡的生命，持續時間
            if (bubbleLife < 0) { Destroy(gameObject); }//當時間<0時，泡泡消滅
        }

        void BigBallLifeReturnToPool()//新版的回到物件池的泡泡
        {
            //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
            if (!gameObject.activeInHierarchy)
                return;

            if (Time.time > _timer + bubbleLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
            {
                GameObject.FindWithTag("BigBallPool").GetComponent<BigBallPool>().Recovery(this.gameObject);
            }
        }
        #endregion

        #region Move
        [Header("Bubble Transform Settings")]
        public float bubbleSpeed;
        public float bubbleSlowDownSpeed;
        private GameObject controller;
        public Vector3 dirToPlayer;
        public int bubbleSpeedRandomMin;
        public int bubbleSpeedRandomMax;

        private void BigBallMove()
        {
            this.transform.position += this.transform.forward * /*這邊控制泡泡的速度*/bubbleSpeed * Time.fixedDeltaTime;
        }
        private void BigBallSlowDownMove()
        {
            this.transform.position += this.transform.forward * bubbleSpeed * Time.fixedDeltaTime;
            bubbleSpeed -= bubbleSlowDownSpeed;
            if (bubbleSpeed < 0)
            {
                bubbleSpeed = 0;
            }
        }

        private void BigBallMovePlayer()//會瞬間移動
        {
            //this.transform.position = Vector3.Lerp(transform.position, (controller.transform.position + controller.transform.up), bubbleSpeed);
            //this.transform.position = controller.transform.position;
            

            this.transform.position += this.transform.forward * bubbleSpeed * Time.fixedDeltaTime;

            bubbleSpeed -= bubbleSlowDownSpeed;
            if (bubbleSpeed < 0)
            {
                bubbleSpeed = 0;
            }


        }

        #endregion

        #region Rotate  泡泡發射口旋轉用的
        [Header("Big Ball Rotate Settings")]
        public GameObject ballEmitterHolder;
        public float rotationSpeed = 0.5f;
        public float rotationAngleX = 30.0f;
        public float rotationAngleY = 0f;
        public float rotationAngleZ = 0f;
        public bool b_EmitterHolderRotate;//要不要讓發射口旋轉
        public void BigBallEmitterRotate()
        {
            if (b_EmitterHolderRotate==false) { return; }
            ballEmitterHolder.transform.Rotate(rotationAngleX, rotationAngleY, rotationAngleZ);
        }
        public void BigBallEmitterRotate(float rotationAngleX, float rotationAngleY, float rotationAngleZ)
        {
            if (b_EmitterHolderRotate==false) { return; }
            ballEmitterHolder.transform.Rotate(rotationAngleX, rotationAngleY, rotationAngleZ);
        }
        #endregion

        #region ShootBubble 這邊是基本款的射砲泡
        [Header("Bubble Shoot Settings")]
        public GameObject[] ballEmitter;//這個不要用
        private void MakeBubbleWithPools()//製造泡泡的，一顆(放在update就每秒60顆)
        {
            for (int i = 0; i < ballEmitter.Length; i++)//多個發射口一齊發射
            {
                pool.ReUse(ballEmitter[i].transform.position, ballEmitter[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }

        public void MakeBubbleWithPools(GameObject[] ballEmittersGroup)//有參數可以丟進去的製造泡泡
        {
            for (int i = 0; i < ballEmittersGroup.Length; i++)//多個發射口一齊發射
            {
                pool.ReUse(ballEmittersGroup[i].transform.position, ballEmittersGroup[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }
        public void MakeBubbleWithSameSpeedPools(GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                sameSpeedPool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }

        //下面存放各種EmitterGroup
        /// <summary>
        /// 15下*8，30下*8，45下*8，60下*8
        /// </summary>
        public GameObject[] ballEmitterGroup1_downShoot;
        /// <summary>
        /// 60度向下 8個
        /// </summary>
        public GameObject[] ballEmitterGroup2_45downshoot;
        /// <summary>
        /// 龍捲風
        /// </summary>
        public GameObject[] ballEmitterGroup3_Ternado;
        /// <summary>
        /// 十字交叉爆炸
        /// </summary>
        public GameObject[] ballEmitterGroup4_Explosion1;
        /// <summary>
        /// 一條像DNA
        /// </summary>
        public GameObject[] ballEmitterGroup5_DNA;
        /// <summary>
        /// 雙圓圈
        /// </summary>
        public GameObject[] ballEmitterGroup6_Circle;
        /// <summary>
        /// 往下射甜甜圈
        /// </summary>
        public GameObject[] ballEmitterGroup7_Donuts;
        
        //下面是有關泡泡射出的速度
        public float bubbleShootSpeed;
        public float bubbleShootCountDown;        

        public float bubbleFirstShootCountDown;//第一次產生泡泡的時間，要配合球縮小到跟泡泡一樣大的時間；但是下面用大球的體積去判斷了，所以好像可以不用這個

        /// <summary>
        /// 射泡泡的，有加計時器，基本款，參考用
        /// </summary>
        public void BubbleShooting()//射泡泡的，有加計時器
        {
            if (bubbleFirstShootCountDown > 0)
            {
                bubbleFirstShootCountDown -= Time.fixedDeltaTime;
            }
            else
            {
               if (bubbleShootCountDown <0)//泡泡發射時間小於0
                {
                    MakeBubbleWithPools();//產生一顆泡泡
                    bubbleShootCountDown = bubbleShootSpeed;//把下次要產生泡泡的時間指定給CountDown，叫它再去扣時間
                }
                else//泡泡發射時間大於0
                {
                    bubbleShootCountDown -= Time.fixedDeltaTime;
                }
            }
        }

        /// <summary>
        /// 射泡泡的，有加計時器，一直射一直射
        /// </summary>
        /// <param name="ballEmittersGroup"></param>
        public void BubbleShooting(GameObject[] ballEmittersGroup)//射泡泡的，有加計時器
        {
            if (bubbleFirstShootCountDown > 0)
            {
                bubbleFirstShootCountDown -= Time.fixedDeltaTime;
            }
            else
            {
                if (bubbleShootCountDown < 0)//泡泡發射時間小於0
                {
                    MakeBubbleWithPools(ballEmittersGroup);//產生一顆泡泡
                    bubbleShootCountDown = bubbleShootSpeed;//把下次要產生泡泡的時間指定給CountDown，叫它再去扣時間
                }
                else//泡泡發射時間大於0
                {
                    bubbleShootCountDown -= Time.fixedDeltaTime;
                }
            }
        }

        /// <summary>
        /// 射泡泡的，有加計時器，等速泡泡，一直射
        /// </summary>
        /// <param name="ballEmittersGroup"></param>
        public void BubbleShootingSameSpeed(GameObject[] ballEmittersGroup)//射泡泡的，有加計時器，等速泡泡
        {
            if (bubbleFirstShootCountDown > 0)
            {
                bubbleFirstShootCountDown -= Time.fixedDeltaTime;
            }
            else
            {
                if (bubbleShootCountDown < 0)//泡泡發射時間小於0
                {
                    MakeBubbleWithSameSpeedPools(ballEmittersGroup);//產生一顆泡泡
                    bubbleShootCountDown = bubbleShootSpeed;//把下次要產生泡泡的時間指定給CountDown，叫它再去扣時間
                }
                else//泡泡發射時間大於0
                {
                    bubbleShootCountDown -= Time.fixedDeltaTime;
                }
            }
        }

        /// <summary>
        /// 射等速泡泡，沒加計時器會很可怕喔
        /// </summary>
        /// <param name="ballEmittersGroup"></param>
        public void BubbleShootingSameSpeedOneTime(GameObject[] ballEmittersGroup)//射泡泡的，沒加計時器，等速泡泡
        {
            if (bubbleFirstShootCountDown > 0)
            {
                bubbleFirstShootCountDown -= Time.fixedDeltaTime;
            }
            else
            {
                if (bubbleShootCountDown < 0)//泡泡發射時間小於0
                {
                    MakeBubbleWithSameSpeedPools(ballEmittersGroup);//產生一顆泡泡
                    bubbleShootCountDown = bubbleShootSpeed;//把下次要產生泡泡的時間指定給CountDown，叫它再去扣時間
                }
                else//泡泡發射時間大於0
                {
                    bubbleShootCountDown -= Time.fixedDeltaTime;
                }
            }
        }

        /// <summary>
        /// 不等速泡泡，沒加計時器
        /// </summary>
        /// <param name="ballEmittersGroup"></param>
        public void BubbleShootingOneTime(GameObject[] ballEmittersGroup)//射泡泡的，沒加計時器
        {
            
                    MakeBubbleWithPools(ballEmittersGroup);//產生一顆泡泡
                    bubbleShootCountDown = bubbleShootSpeed;//把下次要產生泡泡的時間指定給CountDown，叫它再去扣時間
             
        }
        #endregion

        #region Scale transform  這邊放各種球縮小放大
        [Header("Big Ball Scale Settings")]
        public float shrinkSpeed;//縮小的速度，越大越快變小
        public float shrinkSpeedFast;//耗弱
        public GameObject go_DoNotShrinkEmitter;//這邊的不要縮放
        public GameObject go_ShrinkObject;//這邊的要縮放
        public void BigBallShrink()//球縮小
        {
            
            if (bubbleSpeed<0.5)//如果球的速度為0
            {
                //整顆球一起變小
                this.transform.localScale -= new Vector3(0.01f, 0.01f, 0.01f) * shrinkSpeed;
                //go_DoNotShrinkEmitter.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f) * shrinkSpeed;
            }

            if(this.transform.localScale.x <0)//如果小到x軸小於0
            {
                this.transform.localScale = new Vector3(0, 0, 0);//就不要讓它變負的                
            }
            
            shrinkSpeed += shrinkSpeedFast;

        }


        #endregion

        #region Danmaku Pattern 這邊設計各種拋出球的樣式
        [Header("Bubble Shooting Pattern")]
        
        public int bubbleShootPattern;

        private void BubbleShootPatternRandomStart()
        {
            if (this.transform.localScale.x>0.5)//球的體積大於泡泡時，給它一直抽獎，因為一秒更新60次
            {
                bubbleShootPattern = Random.Range(1,21);//1~20
            }
            //else是大球的體積小於泡泡時，就不給它繼續抽，看它最後Random到誰就時誰
            return;//
        }

        public void BubbleShootWithPattern()
        {
            BubbleShootPatternRandomStart();//上面那個Random的函式

            switch (bubbleShootPattern)
            {
                case 6://一秒鐘Random60次，所以會不等速旋轉，但Circle只爆炸一次而已，所以沒差
                    float rotateSpeed=0f;
                    rotateSpeed=Random.Range(1.0f, 10.0f);
                    b_EmitterHolderRotate = true;
                    BigBallEmitterRotate(0f, 0f, rotateSpeed);
                    break;
            }

            if (this.transform.localScale.x < 0.5f)//如果球小到比泡泡還小
            {
                switch (bubbleShootPattern)//Random的結果來決定發射樣式
                {
                    case 1://往下超多發射點亂射  
                    case 5:
                    case 12:
                    case 13:
                    case 14:
                        BubbleShootPattern1();//就開始射泡泡
                        break;
                    case 2:
                        BubbleShootPattern2();//45度角下射的，等速會看起來像錐形
                        break;
                    case 3:
                    case 15:
                    case 18:
                    case 19:
                        BubbleShootPattern3();//愛情來的太快就像龍捲風
                        break;
                    case 4:
                    case 11:
                    case 20:
                        BubbleShootPattern4();//Explosion!!!!!!!
                        break;
                    //case 5:
                    //    BubbleShootPattern5();//DNA
                    //    break;
                    case 6://Circle                        
                    case 9:
                    case 10:
                        BubbleShootPattern6();//Circle
                        break;
                    case 7:
                    case 16:
                        BubbleShootPattern7();//甜甜圈好ㄘ！
                        break;
                    case 8:
                    case 17:
                        BubbleShootPattern8();//往下射圈圈不等速
                        break;
                }
            }
        } 

        private void BubbleShootPattern1()//往下射的大雜燴，很恐怖
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(0f, 1f, 0f);
            bubbleShootSpeed = 0.4f;
            BubbleShooting(ballEmitterGroup1_downShoot);
        }

        private void BubbleShootPattern2()//45度角下射的，等速會看起來像錐形
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(0f,1f,0f);
            bubbleShootSpeed = 0.2f;
            BubbleShootingSameSpeed(ballEmitterGroup2_45downshoot);
        }

        private void BubbleShootPattern3()
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(0f,1f, 0f);
            bubbleShootSpeed = 0.1f;
            BubbleShootingSameSpeed(ballEmitterGroup3_Ternado);
        }

        private void BubbleShootPattern4()
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(1f, 1f, 1f);
            bubbleShootSpeed = 1f;
            BubbleShootingSameSpeed(ballEmitterGroup4_Explosion1);
        }

        private void BubbleShootPattern5()
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(0f, 1f, 0f);
            bubbleShootSpeed = 0.1f;
            BubbleShootingSameSpeed(ballEmitterGroup5_DNA);
        }

        private void BubbleShootPattern6()//爆成一個圓
        {
            //b_EmitterHolderRotate = true;
            //BigBallEmitterRotate(0f, 0f, 1f);
            bubbleShootSpeed = 10f;
            BubbleShootingSameSpeed(ballEmitterGroup6_Circle);
        }

        private void BubbleShootPattern7()//向下射圓圈，像甜甜圈
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(0f, 1f, 0f);
            bubbleShootSpeed = 10.0f;
            BubbleShootingSameSpeed(ballEmitterGroup7_Donuts);
        }

        private void BubbleShootPattern8()
        {
            b_EmitterHolderRotate = true;
            BigBallEmitterRotate(0f, 1f, 0f);
            bubbleShootSpeed = 0.5f;
            BubbleShooting(ballEmitterGroup7_Donuts);
        }
        #endregion

        #region Attack

        /*  //OnTriggerEnter
        public void OnTriggerEnter(Collider other)//這邊寫泡泡撞到的東西會怎樣
        {//這邊只寫，泡泡撞到泡泡不會毀滅，泡泡不會互相影響
            //print(other.gameObject.name);
            if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
            {
                //不做任何事
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//打到子彈圖層，應該還要產生動畫
            {
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Player"))//打到玩家圖層
            {
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))//打到玩家圖層
            {
               
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
            else if (other.gameObject.tag == ("Weapon"))//打到武器圖層
            {
                
                GameObject.FindWithTag("BubblePool").GetComponent<BubblePool>().Recovery(this.gameObject);//回收泡泡，this=泡泡
            }
          

            
        }
        */

        #endregion
    }
}