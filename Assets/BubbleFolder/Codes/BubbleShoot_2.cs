﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bubble
{
    public class BubbleShoot_2 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            BubbleShooting();
            //if (Input.GetKeyDown(KeyCode.P))
            //{
            //    SortedColorBubbles();
            //}
            
        }

        private void OnDrawGizmos()
        {
            //泡泡發射方向
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward);
        }


        [Header("Bubble PreFabs")]
        public GameObject[] bubbles;
        public int bubblesIndex = 0;
        private int randomBubblesIndex;
        public BubblePool pool;

        [Header("Bubble ShootingTime Settings")]
        public float firstShootingCountDown;//第一次射出的時間，不重要，不給初值的話見到玩家馬上發射
        public float resetShootingTime;//重新指派給下一發子彈的射出時間，越小間隔越短，影響到射速
        public float totalWaveShootingTime;//每一波射出時間總長
        public float rechargeTime;//每一波射完之後的等待時間

        public Transform bubbleEmitter;//泡泡的發射口

        [Header("Bubble Timer")]
        [SerializeField] private float shootingTimeCountDown;//要準備發射子彈的倒數時間，監測用
        [SerializeField] private float totalWaveShootingTimeCountDown;//每波射出時間總長的倒數，監測用，這個要給初值才會進入函式
        [SerializeField] private float rechargeTimeCountDown;//等待時間的倒數，要給初值，會影響第一次射完之後的間格，監測用



        //產生泡泡的三種方式
        public void OneColorBubble()//單色泡泡
        {
            GameObject bubbleClone = Instantiate(bubbles[bubblesIndex], bubbleEmitter.transform.position, bubbleEmitter.transform.rotation) as GameObject;
        }
        public void RandomColorBubbles()//隨機顏色泡泡
        {
            randomBubblesIndex = Random.Range(0, bubbles.Length);
            GameObject bubbleClone = Instantiate(bubbles[randomBubblesIndex], bubbleEmitter.transform.position, bubbleEmitter.transform.rotation) as GameObject;
        }
        public void SortedColorBubbles()//順序產生彩色泡泡
        {
            if (bubblesIndex < bubbles.Length)
            {
                GameObject bulletClone = Instantiate(bubbles[bubblesIndex], bubbleEmitter.transform.position, bubbleEmitter.transform.rotation) as GameObject;
                bubblesIndex++;
            }
            else
            {
                bubblesIndex = 0;
            }
        }
        public void MakeBubbleWithPools()
        {
                pool.ReUse(bubbleEmitter.transform.position, bubbleEmitter.transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            
        }


        //射出泡泡的各種方法
        public void BubbleShooting()
        {
            if (totalWaveShootingTimeCountDown >= 0)//如果每一波射出的總時間大於0
            {
                if (firstShootingCountDown > 0)//如果第一次射出時間也大於0
                {
                    firstShootingCountDown -= Time.deltaTime;//則第一次射出時間會減少
                }
                else//如果第一次射出時間小於0
                {
                    if (shootingTimeCountDown > 0)//但子彈射出倒數大於0的話
                    {
                        shootingTimeCountDown -= Time.deltaTime;//子彈射出倒數會減少
                    }
                    else//子彈射出倒數小於0的話
                    {
                        //在泡泡生成點生成一顆泡泡
                        //SortedColorBubbles();
                        //OneColorBubble();
                        MakeBubbleWithPools();

                        shootingTimeCountDown = resetShootingTime;//重新指派時間給子彈倒數
                    }
                }
                //回到上面那邊，整波射出時間>0的話，跑完上面那堆之後給他減少
                totalWaveShootingTimeCountDown -= Time.deltaTime;
            }
            else if (totalWaveShootingTimeCountDown < 0)//當整波的時間小於0
            {
                if (rechargeTimeCountDown > 0)//然後又，再充能時間的倒數大於0的時候
                {
                    rechargeTimeCountDown -= Time.deltaTime;//再充能時間倒數減少
                }
                else//再充能時間的倒數小於0的時候
                {
                    rechargeTimeCountDown = rechargeTime;//再充能時間倒數 重新指派一個時間給他
                    totalWaveShootingTimeCountDown = totalWaveShootingTime;//整波發射時間也給他重新指派
                }
            }
        }


    }

}