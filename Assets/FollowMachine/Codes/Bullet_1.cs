﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class Bullet_1 : MonoBehaviour //現在主要採用這個子彈的腳本
    {
        Collider collider;
        // Start is called before the first frame update
        void Start()
        {
            collider = GetComponent<Collider>();
        }


        private float _timer;//計時器，只會一直增加，比較像是時鐘
        private Transform _myTransform;

        void Awake()
        {
            bulletFire.SetActive(false);
            _myTransform = transform;//初始位置
        }

        void OnEnable()//物件被Active的時候紀錄物件當時的情況
        {
            _timer = Time.time;//開始計時
        }


        void FixedUpdate()
        {
            BulletMove();
            ReturnToPools();
        }


        [Header("Object Pool Sample")]
        //下面這個方法是抄來的範例，如果自己魔改的有出問題的時候參考下面這邊
        public float recoveryTime = 3.0f;//抄來的子彈生命，抄來的是用Time.time+3.0f作為子彈回收的條件
        public float speed = 25;

        void ObjectPoolSample()
        {
            Debug.Log(_timer);
            if (!gameObject.activeInHierarchy)
                return;

            if (Time.time > _timer + recoveryTime)
            {
                GameObject.Find("ObjectPool").GetComponent<ObjectPool>().Recovery(this.gameObject);
            }

            _myTransform.Translate(_myTransform.forward * speed * Time.deltaTime);
        }



        [Header("Bullet Settings")]
        public float bulletLife = 5.0f;//子彈的生命，時間到會被回收
        public float bulletSpeed = 20.0f;//子彈的移動速度，可以加到很快
        public GameObject bulletFire;

        public void BulletMove()//子彈的移動
        {
            this.transform.position += this.transform.forward * bulletSpeed * Time.deltaTime;
            //會自動找尋目標，對著目標射
            //this.transform.position +=  (Sphere1.transform.position-this.transform.position)*bulletSpeed * Time.deltaTime;
        }

        public void ReturnToPools()//生命到，就把子彈回收
        {

            //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
            if (!gameObject.activeInHierarchy)
                return;

            if (Time.time > _timer + bulletLife)//如果現在的遊戲時間大於 物件被Active時的時間+子彈生命時，把子彈回收
            {
                GameObject.Find("BulletPool").GetComponent<BulletPool>().Recovery(this.gameObject);
            }
        }

        private void OnTriggerEnter(Collider other)//子彈打到東西的時候會做什麼
        {
            //把射擊口的火光特效打開
            //bulletFire.SetActive(true);
            //GameObject effect = Instantiate(bulletFire, this.transform.position, Quaternion.identity) as GameObject;//在碰撞處生成一個特效物件
            //Destroy(effect, 1.0f);//一秒鐘之後Destroy該物件，雖然很耗效能...

            if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//子彈打到子彈圖層的時候不做任何事
            {

                //子彈打到子彈不應該做任何事
                Debug.Log("hit bullet");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))//子彈打到泡泡圖層會把泡泡打爆。子彈自己也消失
            {
                //子彈打到泡泡，只回收子彈，並不產生火花特效，泡泡的爆炸特效裝在泡泡身上
                //bulletFire.SetActive(true);
                //GameObject effect = Instantiate(bulletFire, this.transform.position, Quaternion.identity) as GameObject;
                //Destroy(effect, 1.0f);
                GameObject.Find("BulletPool").GetComponent<BulletPool>().Recovery(this.gameObject);
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))//子彈打到主角圖層的東西不做任何事
            {
                //打到玩家圖層不應該做任何事
                collider.enabled = false;
                Debug.Log("hit player");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                bulletFire.SetActive(true);
                GameObject effect = Instantiate(bulletFire, this.transform.position, Quaternion.identity) as GameObject;
                Destroy(effect, 1.0f);
                GameObject.Find("BulletPool").GetComponent<BulletPool>().Recovery(this.gameObject);

            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Weapon"))
            {
                //打到武器圖層不應該做任何事
                Debug.Log("hit weapon");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Default"))//打到其他圖層一樣會爆炸(地形之類的)
            {
                bulletFire.SetActive(true);
                GameObject effect = Instantiate(bulletFire, this.transform.position, Quaternion.identity) as GameObject;
                Destroy(effect, 1.0f);
                GameObject.Find("BulletPool").GetComponent<BulletPool>().Recovery(this.gameObject);
            }
           

            //把射擊口的火花特效關掉
            bulletFire.SetActive(false);
            collider.enabled = true;
        }

        
        


    }
}