// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:4795,x:33531,y:32847,varname:node_4795,prsc:2|emission-2613-OUT,clip-7235-OUT;n:type:ShaderForge.SFN_Color,id:9390,x:32160,y:32637,ptovrint:False,ptlb:node_9390,ptin:_node_9390,varname:node_9390,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:7948,x:32160,y:32798,ptovrint:False,ptlb:node_7948,ptin:_node_7948,varname:node_7948,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.08482099,c4:1;n:type:ShaderForge.SFN_TexCoord,id:8016,x:31904,y:33162,varname:node_8016,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:5107,x:31950,y:33059,ptovrint:False,ptlb:node_5107,ptin:_node_5107,varname:node_5107,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_ComponentMask,id:9610,x:32228,y:33186,varname:node_9610,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-8016-UVOUT;n:type:ShaderForge.SFN_Lerp,id:6029,x:32574,y:32715,varname:node_6029,prsc:2|A-9390-RGB,B-7948-RGB,T-5107-OUT;n:type:ShaderForge.SFN_VertexColor,id:1408,x:32293,y:32936,varname:node_1408,prsc:2;n:type:ShaderForge.SFN_Add,id:7235,x:32597,y:33138,varname:node_7235,prsc:2|A-5107-OUT,B-9610-OUT;n:type:ShaderForge.SFN_Multiply,id:2613,x:32853,y:32780,varname:node_2613,prsc:2|A-6029-OUT,B-1408-RGB;proporder:9390-7948-5107;pass:END;sub:END;*/

Shader "Shader Forge/lightoffset" {
    Properties {
        _node_9390 ("node_9390", Color) = (1,0,0,1)
        _node_7948 ("node_7948", Color) = (0,1,0.08482099,1)
        _node_5107 ("node_5107", Range(-1, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma target 3.0
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _node_9390)
                UNITY_DEFINE_INSTANCED_PROP( float4, _node_7948)
                UNITY_DEFINE_INSTANCED_PROP( float, _node_5107)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                float _node_5107_var = UNITY_ACCESS_INSTANCED_PROP( Props, _node_5107 );
                clip((_node_5107_var+i.uv0.r) - 0.5);
////// Lighting:
////// Emissive:
                float4 _node_9390_var = UNITY_ACCESS_INSTANCED_PROP( Props, _node_9390 );
                float4 _node_7948_var = UNITY_ACCESS_INSTANCED_PROP( Props, _node_7948 );
                float3 emissive = (lerp(_node_9390_var.rgb,_node_7948_var.rgb,_node_5107_var)*i.vertexColor.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
